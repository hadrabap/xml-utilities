/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.io.BufferedReader;
import java.io.StringReader;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author petr
 */
public class PrintStackTrace extends ExceptionTestBase {

    @Test
    public void sameStactTrace() {
        XmlUtilitiesException actual = createXmlUtilitiesException("XmlUtilitiesException", null, null);
        Exception expected = createGenericException("e1", null);
        Assert.assertEquals(lineCount(throwableToString(expected)), lineCount(throwableToString(actual)));
    }

    @Test
    public void consequentCount() {
        XmlUtilitiesException ex = createXmlUtilitiesException("XXX", null, new Throwable[] {
            new Exception("e1"), new Exception("e2")
        });
        int actual = ex.getConsequent().length;
        int expected = 2;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void consequestsInStackTrace() throws Exception {
        XmlUtilitiesException ex = createXmlUtilitiesException("XXX", null, new Throwable[] {
            new Exception("e1"), new Exception("e2")
        });
        String stackTrace = throwableToString(ex);

        StringReader sr = new StringReader(stackTrace);
        BufferedReader br = new BufferedReader(sr);
        int occurence = 0;
        String line = null;
        while ((line = br.readLine()) != null) {
            if (line.toLowerCase().contains("consequent")) {
                occurence++;
            }
        }

        int expected = 2;
        Assert.assertEquals(expected, occurence);
    }

    private XmlUtilitiesException createXmlUtilitiesException(String message, Throwable cause, Throwable[] consequents) {
        XmlUtilitiesException result = new XmlUtilitiesException(message, cause);
        if (consequents != null) {
            for (Throwable consequent : consequents) {
                result.addConsequent(consequent);
            }
        }
        return result;
    }

    private Exception createGenericException(String message, Throwable cause) {
        return new Exception(message, cause);
    }

}
