/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.io.InputStream;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.w3c.dom.Document;

/**
 *
 * @author petr
 */
public abstract class XmlUtilitiesTestBase {

    protected XmlUtilities xmlUtilities;

    protected NamespaceContextImpl namespaceContext;

    @Before
    public void setUp() throws Exception {
        xmlUtilities = new XmlUtilities();
        namespaceContext = createNamespaceContextImpl();
        XmlTools.resetXmlUtilities();
    }

    @After
    public void tearDown() throws Exception {
        namespaceContext = null;
        xmlUtilities = null;
    }

    protected Document loadSchema() throws Exception {
        return loadDocument("sample.xsd");
    }

    protected Document loadDocument(String name) throws Exception {
        InputStream is = getClass().getResourceAsStream(name);

        try {
            if (is == null) {
                Assert.fail(String.format("The resource %s not found on classpath.", name));
            }
            return xmlUtilities.loadDocumentFromStream(is);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private NamespaceContextImpl createNamespaceContextImpl() throws Exception {
        NamespaceContextImpl nsctx = NamespaceContextImpl.namespaceContextWithDefaults();

        nsctx.addNamespace("ns", "http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd");

        return nsctx;
    }

}
