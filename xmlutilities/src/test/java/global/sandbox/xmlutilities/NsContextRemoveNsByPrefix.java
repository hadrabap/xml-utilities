/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import javax.xml.XMLConstants;
import org.junit.Assert;
import org.junit.Test;

public class NsContextRemoveNsByPrefix {

    @Test
    public void testRemoveByPrefix() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();
        nsCtx.addNamespace("p1", "http://global.sandbox/p1");

        Assert.assertEquals("p1", nsCtx.getPrefix("http://global.sandbox/p1"));
        nsCtx.removeNamespaceByPrefix("p1");
        Assert.assertNull(nsCtx.getPrefix("http://global.sandbox/p1"));
    }

    @Test
    public void testRemoveNonExisting() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.addNamespace(XMLConstants.XML_NS_PREFIX, XMLConstants.XML_NS_URI);

        Assert.assertEquals(XMLConstants.XML_NS_URI, nsCtx.getNamespaceURI(XMLConstants.XML_NS_PREFIX));
        nsCtx.removeNamespaceByPrefix("no:such:prefix");
        Assert.assertEquals(XMLConstants.XML_NS_URI, nsCtx.getNamespaceURI(XMLConstants.XML_NS_PREFIX));
    }

    @Test
    public void testIgnoreNullValue() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.removeNamespaceByPrefix(null);
    }

}
