/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests.xmlutilities;

import global.sandbox.xmlutilities.XmlUtilities;
import global.sandbox.xmlutilities.tests.ResourceEnabledTestBase;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author petr
 */
public abstract class XmlUtilitiesTestBase extends ResourceEnabledTestBase {

    protected XmlUtilities xmlUtilities;

    @Before
    public void setUpTestXmlUtilities() throws Exception {
        this.xmlUtilities = new XmlUtilities();
    }

    @After
    public void tearDownXmlUtilities() throws Exception {
        this.xmlUtilities = null;
    }

}
