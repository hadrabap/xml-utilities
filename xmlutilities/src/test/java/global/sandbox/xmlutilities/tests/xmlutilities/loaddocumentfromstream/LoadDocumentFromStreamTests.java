/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests.xmlutilities.loaddocumentfromstream;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.tests.xmlutilities.XmlUtilitiesTestBase;
import java.io.InputStream;
import java.util.logging.Level;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author petr
 */
public class LoadDocumentFromStreamTests extends XmlUtilitiesTestBase {

    @Test
    public void defaultStream() throws Exception {
        TestCaseSetup tcs = setupTestCase();

        InputStream testDocument = tcs.getTestDocumentStream();
        Document doc = xmlUtilities.loadDocumentFromStream(testDocument);
        testDocument.close();

        NodeList propertyElements = doc.getDocumentElement().getElementsByTagNameNS("http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd", "property");
        if (propertyElements == null || propertyElements.getLength() == 0) {
            Assert.fail("No property elements found.");
            return;
        }
        for (int i = 0; i < propertyElements.getLength(); i++) {
            Element propertyElement = (Element) propertyElements.item(i);
            LOGGER.log(Level.INFO, "Element contents is ''{0}''.", propertyElement.getTextContent());
            switch (i) {
                case 0:
                    Assert.assertEquals("ěščřžýáíéůú", propertyElement.getTextContent());
                    break;
                case 1:
                    Assert.assertEquals("ĚŠČŘŽÝÁÍÉŮÚ", propertyElement.getTextContent());
                    break;
                default:
                    Assert.fail(String.format("Unexpected index %d.", i));
            }
        }
    }

    @Test
    public void differentEncoding() throws Exception {
        TestCaseSetup tcs = setupTestCase();

        InputStream testDocument = tcs.getTestDocumentStream();
        Document doc = xmlUtilities.loadDocumentFromStream(testDocument);
        testDocument.close();

        NodeList propertyElements = doc.getDocumentElement().getElementsByTagNameNS("http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd", "property");
        if (propertyElements == null || propertyElements.getLength() == 0) {
            Assert.fail("No property elements found.");
            return;
        }
        for (int i = 0; i < propertyElements.getLength(); i++) {
            Element propertyElement = (Element) propertyElements.item(i);
            LOGGER.log(Level.INFO, "Element contents is ''{0}''.", propertyElement.getTextContent());
            switch (i) {
                case 0:
                    Assert.assertEquals("ěščřžýáíéůú", propertyElement.getTextContent());
                    break;
                case 1:
                    Assert.assertEquals("ĚŠČŘŽÝÁÍÉŮÚ", propertyElement.getTextContent());
                    break;
                default:
                    Assert.fail(String.format("Unexpected index %d.", i));
            }
        }
    }

}
