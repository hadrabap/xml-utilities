/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests.xmltools.writedocumenttofile;

import global.sandbox.xmlutilities.OutputFormat;
import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.tests.xmltools.XmlToolsTestBase;
import java.nio.charset.Charset;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

/**
 *
 * @author petr
 */
public class WriteDocumentToFileTests extends XmlToolsTestBase {

    private static final Charset ISO_8859_2 = Charset.forName("ISO-8859-2");

    @Test
    public void defaultFile() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();

        XmlTools.writeDocumentToFile(testDocument, tcs.getTemporaryFile());

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void defaultString() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();

        XmlTools.writeDocumentToFile(testDocument, tcs.getTemporaryFile().toString());

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void outputFormatFile() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        testDocument.setXmlStandalone(true);

        OutputFormat of = OutputFormat.newXmlMethodBuilder()
                .setStandalone(OutputFormat.Boolean.TRUE)
                .build();

        XmlTools.writeDocumentToFile(testDocument, tcs.getTemporaryFile(), of);

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void outputFormatString() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        testDocument.setXmlStandalone(true);

        OutputFormat of = OutputFormat.newXmlMethodBuilder()
                .setStandalone(OutputFormat.Boolean.TRUE)
                .build();

        XmlTools.writeDocumentToFile(testDocument, tcs.getTemporaryFile().toString(), of);

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void propertiesFile() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        Document doc = internalXmlUtilities.getDocumentBuilder().newDocument();
        doc.appendChild(doc.adoptNode(testDocument.getDocumentElement()));

        Properties properties = OutputFormat.newXmlMethodBuilder()
                .setEncoding(ISO_8859_2.name())
                .buildToTransformerOutputProperties();

        XmlTools.writeDocumentToFile(doc, tcs.getTemporaryFile(), properties);

        String actualResult = tcs.getActualResult(ISO_8859_2);
        String expectedResult = tcs.getExpectedResult(ISO_8859_2);

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void propertiesString() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        Document doc = internalXmlUtilities.getDocumentBuilder().newDocument();
        doc.appendChild(doc.adoptNode(testDocument.getDocumentElement()));

        Properties properties = OutputFormat.newXmlMethodBuilder()
                .setEncoding(ISO_8859_2.name())
                .buildToTransformerOutputProperties();

        XmlTools.writeDocumentToFile(doc, tcs.getTemporaryFile().toString(), properties);

        String actualResult = tcs.getActualResult(ISO_8859_2);
        String expectedResult = tcs.getExpectedResult(ISO_8859_2);

        Assert.assertEquals(expectedResult, actualResult);
    }

}
