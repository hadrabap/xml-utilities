/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests;

import global.sandbox.xmlutilities.NamespaceContextImpl;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author petr
 */
public abstract class NamespaceContextTestBase extends CommonTestBase {

    protected NamespaceContextImpl namespaceContext;

    @Before
    public void setUp() throws Exception {
        namespaceContext = createNamespaceContextImpl();
    }

    @After
    public void tearDown() throws Exception {
        namespaceContext = null;
    }

    private NamespaceContextImpl createNamespaceContextImpl() throws Exception {
        NamespaceContextImpl nsctx = NamespaceContextImpl.namespaceContextWithDefaults();

        nsctx.addNamespace("ns", "http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd");

        return nsctx;
    }

}
