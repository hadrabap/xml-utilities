/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests.xmlutilities.loaddocumentfromstring;

import global.sandbox.xmlutilities.tests.xmlutilities.XmlUtilitiesTestBase;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author petr
 */
public class LoadDocumentFromStringTests extends XmlUtilitiesTestBase {

    @Test
    public void defaultString() throws Exception {
        Document doc = xmlUtilities.loadDocumentFromString(getExpectedResult("defaultString.xml"));

        Assert.assertNotNull(doc);

        NodeList propertyElements = doc.getDocumentElement().getElementsByTagNameNS("http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd", "property");
        if (propertyElements == null || propertyElements.getLength() != 2) {
            Assert.fail("ns:property not found exactly two times.");
            return;
        }
        for (int i = 0; i < propertyElements.getLength(); i++) {
            Element property = (Element) propertyElements.item(i);

            switch (i) {
                case 0:
                    Assert.assertEquals("ěščřžýáíéůú", property.getTextContent());
                    break;
                case 1:
                    Assert.assertEquals("ĚŠČŘŽÝÁÍÉŮÚ", property.getTextContent());
                    break;
                default:
                    Assert.fail(String.format("Out of range %d (0-1).", i));
                    return;
            }
        }
    }

}
