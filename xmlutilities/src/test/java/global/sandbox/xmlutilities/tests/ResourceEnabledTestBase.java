/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests;

import global.sandbox.xmlutilities.XmlUtilitiesException;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.logging.Level;
import org.junit.Assert;
import org.w3c.dom.Document;

import static global.sandbox.xmlutilities.tests.CommonTestBase.DEFAULT_CHARSET;

/**
 *
 * @author petr
 */
public abstract class ResourceEnabledTestBase extends FileSystemTestBase {

    protected String getExpectedResult(final String resourceName) throws IOException {
        return getExpectedResult(resourceName, DEFAULT_CHARSET);
    }

    protected String getExpectedResult(final String resourceName, final Charset charset) throws IOException {
        Assert.assertNotNull(resourceName);
        Assert.assertNotNull(charset);
        LOGGER.log(Level.INFO, "About to load expected resource ''{0}'' using class ''{1}'' and encoding ''{2}''.",
                new Object[] { resourceName, thisClass.toString(), charset.name() });
        InputStream resourceAsStream = thisClass.getResourceAsStream(resourceName);
        Assert.assertNotNull(String.format("The resource '%s' not found.", resourceName), resourceAsStream);
        BufferedReader br = new BufferedReader(new InputStreamReader(resourceAsStream, charset));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                sb.append(line).append(LINE_SEPARATOR);
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }

    protected Document getTestDocument(final String resourceName) throws XmlUtilitiesException {
        Assert.assertNotNull(resourceName);
        LOGGER.log(Level.INFO, "About to load test document from resource ''{0}'' using class ''{1}''.", new Object[] { resourceName, thisClass });
        return internalXmlUtilities.loadDocumentFromResource(resourceName, thisClass);
    }

    protected InputStream getTestDocumentStream(final String resourceName) {
        LOGGER.log(Level.INFO, "About to return resource ''{0}'' as InputStream using class ''{1}''.", new Object[] { resourceName, thisClass });
        InputStream resourceAsStream = thisClass.getResourceAsStream(resourceName);
        Assert.assertNotNull(String.format("Resource '%s' not found.", resourceName), resourceAsStream);
        return resourceAsStream;
    }

    protected TestCaseSetup setupTestCase() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        Assert.assertTrue("Invalid call.", stackTrace.length > 3);
        final String methodName = stackTrace[2].getMethodName();
        LOGGER.log(Level.INFO, "Method name: ''{0}''.", methodName);
        TestCaseSetup tcs = new TestCaseSetup(methodName, methodName);
        return tcs;
    }

    protected File createTestFileFromResource(final String resourceName) throws IOException {
        Assert.assertNotNull(resourceName);
        LOGGER.log(Level.INFO, "About to create test file from resource ''{0}''.", resourceName);
        InputStream resourceAsStream = thisClass.getResourceAsStream(resourceName);
        Assert.assertNotNull(String.format("The resource '%s' not found!", resourceName), resourceAsStream);
        File testFile = generateTempFile(resourceName);
        try {
            IOUtils.copy(resourceAsStream, testFile);
        } finally {
            resourceAsStream.close();
        }
        return testFile;
    }

    protected class TestCaseSetup {

        private final String methodName;

        private final String resourcePrefix;

        private final File temporaryFile;

        private OutputStream temporaryOutputStream;

        private TestCaseSetup(String methodName, String resourcePrefix) {
            this.methodName = methodName;
            this.resourcePrefix = resourcePrefix;
            this.temporaryFile = ResourceEnabledTestBase.this.generateTempFile(resourcePrefix);
        }

        public String getMethodName() {
            return methodName;
        }

        public String getResourcePrefix() {
            return resourcePrefix;
        }

        public Document getTestDocument() throws XmlUtilitiesException {
            return ResourceEnabledTestBase.this.getTestDocument(getTestDocumentResourceName());
        }

        public String getExpectedResult() throws IOException {
            return ResourceEnabledTestBase.this.getExpectedResult(getExpectedResultResourceName());
        }

        public String getExpectedResult(Charset charset) throws IOException {
            return ResourceEnabledTestBase.this.getExpectedResult(getExpectedResultResourceName(), charset);
        }

        public File getTemporaryFile() {
            return temporaryFile;
        }

        public String getActualResult() throws IOException {
            return ResourceEnabledTestBase.this.getActualResult(temporaryFile);
        }

        public String getActualResult(Charset charset) throws IOException {
            return ResourceEnabledTestBase.this.getActualResult(temporaryFile, charset);
        }

        public OutputStream createTemporaryOutputStream() throws FileNotFoundException {
            Assert.assertNull(this.temporaryOutputStream);
            this.temporaryOutputStream = new FileOutputStream(temporaryFile);
            return this.temporaryOutputStream;
        }

        public void closeTemporaryOutputStream() throws IOException {
            Assert.assertNotNull(this.temporaryOutputStream);
            this.temporaryOutputStream.flush();
            this.temporaryOutputStream.close();
            this.temporaryOutputStream = null;
        }

        public InputStream getTestDocumentStream() {
            return ResourceEnabledTestBase.this.getTestDocumentStream(getTestDocumentResourceName());
        }

        private String getTestDocumentResourceName() {
            return String.format("%s.xml", resourcePrefix);
        }

        private String getExpectedResultResourceName() {
            return String.format("%s.expected.bin", resourcePrefix);
        }

    }

    private static final class IOUtils {

        private static final int DEFAULT_BUFFER_SIZE = 4096;

        public static void copy(final InputStream is, final OutputStream os) throws IOException {
            byte[] buffer = new byte[DEFAULT_BUFFER_SIZE];
            int read;
            while ((read = is.read(buffer)) != -1) {
                os.write(buffer, 0, read);
            }
        }

        public static void copy(final InputStream is, final File file) throws FileNotFoundException, IOException {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file), DEFAULT_BUFFER_SIZE);
            try {
                copy(is, bos);
            } finally {
                bos.flush();
                bos.close();
            }
        }

        private IOUtils() {
            throw new AssertionError("No instance of " + IOUtils.class.getName() + " for you!");
        }

    }
}
