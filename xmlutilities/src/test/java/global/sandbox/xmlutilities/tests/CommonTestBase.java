/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilities;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import java.nio.charset.Charset;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author petr
 */
public abstract class CommonTestBase {

    protected final Logger LOGGER;

    protected final Class<?> thisClass;

    protected XmlUtilities internalXmlUtilities;

    protected static final Charset DEFAULT_CHARSET;

    protected static final String LINE_SEPARATOR;

    static {
        DEFAULT_CHARSET = Charset.forName("UTF-8");
        LINE_SEPARATOR = System.getProperty("line.separator", "\n");
    }

    public CommonTestBase() {
        this.thisClass = this.getClass();
        this.LOGGER = Logger.getLogger(thisClass.getName());
    }

    @Before
    public void setUpCommonTestBase() throws XmlUtilitiesException {
        XmlTools.resetXmlUtilities();
        internalXmlUtilities = new XmlUtilities();
    }

    @After
    public void tearDownCommonTestBase() {
        XmlTools.resetXmlUtilities();
        internalXmlUtilities = null;
    }

}
