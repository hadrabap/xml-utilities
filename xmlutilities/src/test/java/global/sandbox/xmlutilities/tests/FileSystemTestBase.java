/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.UUID;
import java.util.logging.Level;
import org.junit.Assert;

/**
 *
 * @author petr
 */
public abstract class FileSystemTestBase extends NamespaceContextTestBase {

    protected static final String DEFAULT_XML_EXTENSION = "xml";

    private static final String WORK_DIRECTORY_NAME = "work";

    protected File getTargetDirectory() {
        final File target = new File("target").getAbsoluteFile();
        Assert.assertTrue(String.format("The target directory '%s' does not exist.", target.toString()), target.exists());
        Assert.assertTrue(String.format("The target directory '%s' is not writable.", target.toString()), target.canWrite());
        LOGGER.log(Level.INFO, "Target directory is {0}.", target.toString());
        return target;
    }

    protected File getWorkDirectory() {
        final File target = getTargetDirectory();
        final File work = new File(target, WORK_DIRECTORY_NAME);

        if (!work.exists()) {
            LOGGER.log(Level.INFO, "The work directory ''{0}'' does not exist; creating.", work.toString());
            Assert.assertTrue(String.format("Failed to create work directory '%s'.", work.toString()), work.mkdir());
        } else {
            LOGGER.log(Level.INFO, "Work directory ''{0}'' already exists.", work.toString());
        }

        return work;
    }

    protected String getActualResult(final File tempFile) throws IOException {
        return getActualResult(tempFile, DEFAULT_CHARSET);
    }

    protected String getActualResult(final File tempFile, final Charset charset) throws IOException {
        Assert.assertNotNull(tempFile);
        Assert.assertNotNull(charset);
        LOGGER.log(Level.INFO, "About to load actual result from file ''{0}'' using encoding ''{1}''.", new Object[] { tempFile.toString(), charset.name() });
        Assert.assertTrue(String.format("The file '%s' does not exist.", tempFile.toString()), tempFile.exists());
        Assert.assertTrue(String.format("The file '%s' is not readable.", tempFile.toString()), tempFile.canRead());
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(tempFile), charset));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                sb.append(line).append(LINE_SEPARATOR);
            }
        } finally {
            br.close();
        }
        return sb.toString();
    }

    protected File generateTempFile(final String prefix) {
        Assert.assertNotNull(prefix);
        LOGGER.log(Level.INFO, "About to generate temporary file with prefix ''{0}'' and default extension ({1}).", new Object[] { prefix, DEFAULT_XML_EXTENSION });
        final File tempFile = generateTempFile0(prefix, DEFAULT_XML_EXTENSION);
        return tempFile;
    }

    protected File generateTempFile(final String prefix, final String extension) {
        Assert.assertNotNull(prefix);
        Assert.assertNotNull(extension);
        LOGGER.log(Level.INFO, "About to generate temporary file with prefix ''{0}'' and extension ''{1}''.", new Object[] { prefix, extension });
        final File tempFile = generateTempFile0(prefix, extension);
        return tempFile;
    }

    private File generateTempFile0(final String prefix, final String extension) {
        final String fileName = generateTempFileName(prefix, extension);
        final File tempFile = new File(getWorkDirectory(), fileName);
        Assert.assertFalse(String.format("The temporary file '%s' should not exist.", tempFile.toString()), tempFile.exists());
        LOGGER.log(Level.INFO, "Temporary file is ''{0}''.", tempFile.toString());
        return tempFile;
    }

    private String generateTempFileName(final String prefix, final String extension) {
        final String fileName = String.format("%s-%s.%s", prefix, UUID.randomUUID().toString(), extension);
        LOGGER.log(Level.INFO, "Generated file name is ''{0}''.", fileName);
        return fileName;
    }

}
