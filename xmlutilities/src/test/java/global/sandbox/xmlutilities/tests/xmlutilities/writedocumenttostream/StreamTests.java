/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities.tests.xmlutilities.writedocumenttostream;

import global.sandbox.xmlutilities.OutputFormat;
import global.sandbox.xmlutilities.tests.xmlutilities.XmlUtilitiesTestBase;
import java.nio.charset.Charset;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;

/**
 *
 * @author petr
 */
public class StreamTests extends XmlUtilitiesTestBase {

    private static final Charset ISO_8859_2 = Charset.forName("ISO-8859-2");

    @Test
    public void defaultStream() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();

        xmlUtilities.writeDocumentToStream(testDocument, tcs.createTemporaryOutputStream());
        tcs.closeTemporaryOutputStream();

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void outputFormat() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        testDocument.setXmlStandalone(true);

        OutputFormat of = OutputFormat.newXmlMethodBuilder()
                .setStandalone(OutputFormat.Boolean.TRUE)
                .build();

        xmlUtilities.writeDocumentToStream(testDocument, tcs.createTemporaryOutputStream(), of);
        tcs.closeTemporaryOutputStream();

        String actualResult = tcs.getActualResult();
        String expectedResult = tcs.getExpectedResult();

        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void properties() throws Exception {
        TestCaseSetup tcs = setupTestCase();
        Document testDocument = tcs.getTestDocument();
        Document doc = internalXmlUtilities.getDocumentBuilder().newDocument();
        doc.appendChild(doc.adoptNode(testDocument.getDocumentElement()));

        Properties properties = OutputFormat.newXmlMethodBuilder()
                .setEncoding(ISO_8859_2.name())
                .buildToTransformerOutputProperties();

        xmlUtilities.writeDocumentToStream(doc, tcs.createTemporaryOutputStream(), properties);
        tcs.closeTemporaryOutputStream();

        String actualResult = tcs.getActualResult(ISO_8859_2);
        String expectedResult = tcs.getExpectedResult(ISO_8859_2);

        Assert.assertEquals(expectedResult, actualResult);
    }

}
