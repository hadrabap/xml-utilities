/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import javax.xml.XMLConstants;
import org.junit.Assert;
import org.junit.Test;

public class NsContextGetNamespaceUri {

    @Test
    public void testGetNamespaceUri() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();

        Assert.assertEquals(XMLConstants.XML_NS_URI, nsCtx.getNamespaceURI(XMLConstants.XML_NS_PREFIX));
    }

    @Test
    public void testNoSuchPrefix() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();

        Assert.assertEquals(XMLConstants.NULL_NS_URI, nsCtx.getNamespaceURI("no/such/prefix"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidUsage() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();
        nsCtx.getNamespaceURI(null);
    }

}
