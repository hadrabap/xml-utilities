/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import javax.xml.XMLConstants;
import org.junit.Assert;
import org.junit.Test;

public class NsContextGetPrefix {

    @Test
    public void testGetExisting() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();
        Assert.assertEquals(XMLConstants.XML_NS_PREFIX, nsCtx.getPrefix(XMLConstants.XML_NS_URI));
    }

    @Test
    public void testGetNonExisting() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();
        Assert.assertNull(nsCtx.getPrefix("this/does/not/exist"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidUsage() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.namespaceContextWithDefaults();
        nsCtx.getPrefix(null);
    }

}
