/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 *
 * @author petr
 */
abstract class ExceptionTestBase {

    public String throwableToString(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        return sw.toString();
    }

    public int lineCount(final String s) {
        if (s == null) {
            return 0;
        }

        if (s.isEmpty()) {
            return 0;
        }

        int lines = 1;
        for (int pos = 0; pos < s.length(); pos++) {
            final char c = s.charAt(pos);
            switch (c) {
                case '\r':
                    lines++;
                    if (pos + 1 < s.length() && s.charAt(pos + 1) == '\n') {
                        pos++;
                    }
                    break;
                case '\n':
                    lines++;
                    break;
                default:
                    break;
            }
        }

        if (s.endsWith("\r") || s.endsWith("\n")) {
            lines--;
        }

        return lines;
    }

}
