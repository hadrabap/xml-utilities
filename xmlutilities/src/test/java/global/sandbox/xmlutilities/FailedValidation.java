/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;


/**
 *
 * @author petr
 */
public class FailedValidation extends XmlUtilitiesTestBase {

    private static final Logger LOGGER = Logger.getLogger(FailedValidation.class.getName());

    @Test
    public void failedValidation() throws Exception {
        Document schema = loadSchema();
        Document invalidXML = loadDocument("invalid.xml");

        String result = xmlUtilities.validateXmlUsingSchema(
                XmlTools.documentToDomSource(invalidXML),
                XmlTools.documentToDomSource(schema));

        LOGGER.log(Level.INFO, "The resulting String contains error message if problem found during validation process: {0}", result);

        Assert.assertNotNull(result);
    }

}
