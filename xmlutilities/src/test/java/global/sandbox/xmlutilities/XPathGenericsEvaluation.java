/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 *
 * @author petr
 */
public class XPathGenericsEvaluation extends XmlUtilitiesTestBase {

    @Test
    public void testEvaluateXPath_1() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Element> elements = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property",
                validXML,
                namespaceContext,
                Element.class);

        Assert.assertEquals(
                2,
                elements.size());
    }

    @Test
    public void testEvaluateXPath_Types() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Element> elements = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property",
                validXML,
                namespaceContext,
                Element.class);

        for (Element e : elements) {
            Assert.assertTrue(Element.class.isAssignableFrom(e.getClass()));
        }
    }

    @Test
    public void testEvaluateXPathTextContent_NumberOfResults() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Text> texts = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property/text()",
                validXML,
                namespaceContext,
                Text.class);

        Assert.assertEquals(
                2,
                texts.size());
    }

    @Test
    public void testEvaluateXPathTextContent_Values() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Text> texts = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property/text()",
                validXML,
                namespaceContext,
                Text.class);

        int i = 0;
        for (Text text : texts) {
            switch (i) {
                case 0:
                    Assert.assertEquals("p1 value", text.getTextContent());
                    break;
                case 1:
                    Assert.assertEquals("p2 value", text.getTextContent());
                    break;
                default:
                    Assert.fail(String.format("Unsupported index %d.", i));
                    break;
            }
            i++;
        }
    }

    @Test
    public void testEvaluateXPathTextContent_Types() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Text> texts = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property/text()",
                validXML,
                namespaceContext,
                Text.class);

        for (Text text : texts) {
            Assert.assertTrue(Text.class.isAssignableFrom(text.getClass()));
        }
    }

    @Test
    public void testEvaluateXPathAttr() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Attr> attrs = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property/@name",
                validXML,
                namespaceContext,
                Attr.class);

        Assert.assertEquals(
                2,
                attrs.size());
    }

    @Test
    public void testEvaluateXPathAttr_Types() throws Exception {
        Document validXML = loadDocument("valid.xml");

        List<Attr> attrs = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property/@name",
                validXML,
                namespaceContext,
                Attr.class);

        for (Attr a : attrs) {
            Assert.assertTrue(Attr.class.isAssignableFrom(a.getClass()));
        }
    }

}
