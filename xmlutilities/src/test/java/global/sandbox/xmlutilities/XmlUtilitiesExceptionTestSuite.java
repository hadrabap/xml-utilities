/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author petr
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    ExceptionTestBaseTest.class,
    PrintStackTrace.class
})
public class XmlUtilitiesExceptionTestSuite {

}
