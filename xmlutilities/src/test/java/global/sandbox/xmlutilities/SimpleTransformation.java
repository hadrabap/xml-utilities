/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.transform.Source;
import org.junit.Assert;
import org.junit.Test;


/**
 *
 * @author petr
 */
public class SimpleTransformation extends XmlUtilitiesTestBase {

    private static final Logger LOGGER = Logger.getLogger(SimpleTransformation.class.getName());

    @Test
    public void transform() throws Exception {
        Source validXML = XmlTools.documentToDomSource(loadDocument("valid.xml"));
        Source style = XmlTools.documentToDomSource(loadDocument("style.xsl"));

        String result = xmlUtilities.transformToString(
                style,
                validXML);

        LOGGER.log(Level.INFO, "Transformed properties: {0}", result);

        Assert.assertNotNull(result);
    }

}
