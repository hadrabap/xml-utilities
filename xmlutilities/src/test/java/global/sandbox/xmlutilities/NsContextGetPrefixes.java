/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class NsContextGetPrefixes {

    @Test
    public void testGetPrefixes() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.addNamespace("p1", "http://global.sandbox/p1");
        nsCtx.addNamespace("p2", "http://global.sandbox/p2");

        Assert.assertEquals("p1", nsCtx.getPrefixes("http://global.sandbox/p1").next());
        Assert.assertEquals("p2", nsCtx.getPrefixes("http://global.sandbox/p2").next());
    }

    @Test
    public void testGetSamePrefixes() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.addNamespace("p1", "http://global.sandbox/p");
        nsCtx.addNamespace("p2", "http://global.sandbox/p");

        List<String> expected = new ArrayList<String>(Arrays.asList("p1", "p2"));
        Iterator<String> prefixes = nsCtx.getPrefixes("http://global.sandbox/p");
        while (prefixes.hasNext()) {
            final String prefix = prefixes.next();

            Assert.assertTrue(expected.contains(prefix));
            expected.remove(prefix);
        }
        Assert.assertTrue(expected.isEmpty());
    }

    @Test
    public void testNoPrefix() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();

        Assert.assertFalse(nsCtx.getPrefixes("no/such/prefix/for/this/URI").hasNext());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidUsage() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.getPrefixes(null);
    }

}
