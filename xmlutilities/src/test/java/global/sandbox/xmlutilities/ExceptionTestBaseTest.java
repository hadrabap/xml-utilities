/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author petr
 */
public class ExceptionTestBaseTest extends ExceptionTestBase {

    @Test
    public void twoLines_LF() {
        String test = "Hello\nWorld\n";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void twoLinesNoEol_LF() {
        String test = "Hello\nWorld";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void emptyLine_LF() {
        String test = "\n";
        Assert.assertEquals(1, lineCount(test));
    }

    @Test
    public void noLines() {
        String test = "";
        Assert.assertEquals(0, lineCount(test));
    }

    @Test
    public void threeLines_LF() {
        String test = "Hello\n\nWorld";
        Assert.assertEquals(3, lineCount(test));
    }

    @Test
    public void fourLines_LF() {
        String test = "Hello\n\nWorld\n\n";
        Assert.assertEquals(4, lineCount(test));
    }

    @Test
    public void fiveLines_LF() {
        String test = "Hello\n\nWorld\n\n ";
        Assert.assertEquals(5, lineCount(test));
    }

    @Test
    public void twoLines_CRLF() {
        String test = "Hello\r\nWorld\r\n";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void twoLinesNoEol_CRLF() {
        String test = "Hello\r\nWorld";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void emptyLine_CRLF() {
        String test = "\r\n";
        Assert.assertEquals(1, lineCount(test));
    }

    @Test
    public void threeLines_CRLF() {
        String test = "Hello\r\n\r\nWorld";
        Assert.assertEquals(3, lineCount(test));
    }

    @Test
    public void fourLines_CRLF() {
        String test = "Hello\r\n\r\nWorld\r\n\r\n";
        Assert.assertEquals(4, lineCount(test));
    }

    @Test
    public void fiveLines_CRLF() {
        String test = "Hello\r\n\r\nWorld\r\n\r\n ";
        Assert.assertEquals(5, lineCount(test));
    }

    @Test
    public void twoLines_CR() {
        String test = "Hello\rWorld\r";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void twoLinesNoEol_CR() {
        String test = "Hello\rWorld";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void emptyLine_CR() {
        String test = "\r";
        Assert.assertEquals(1, lineCount(test));
    }

    @Test
    public void threeLines_CR() {
        String test = "Hello\r\rWorld";
        Assert.assertEquals(3, lineCount(test));
    }

    @Test
    public void fourLines_CR() {
        String test = "Hello\r\rWorld\r\r";
        Assert.assertEquals(4, lineCount(test));
    }

    @Test
    public void fiveLines_CR() {
        String test = "Hello\r\rWorld\r\r ";
        Assert.assertEquals(5, lineCount(test));
    }

    @Test
    public void twoLines_MIX() {
        String test = "Hello\rWorld\n";
        Assert.assertEquals(2, lineCount(test));
    }

    @Test
    public void threeLines_MIX() {
        String test = "Hello\n\rWorld";
        Assert.assertEquals(3, lineCount(test));
    }

    @Test
    public void fourLines_MIX() {
        String test = "Hello\n\nWorld\n\r";
        Assert.assertEquals(4, lineCount(test));
    }

    @Test
    public void fiveLines_MIX() {
        String test = "Hello\n\rWorld\n\r ";
        Assert.assertEquals(5, lineCount(test));
    }

}
