/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author petr
 */
public class XPathEvaluation extends XmlUtilitiesTestBase {

    @Test
    public void testEvaluateXPath_1() throws Exception {
        Document validXML = loadDocument("valid.xml");

        NodeList elements = xmlUtilities.evaluateXPath(
                "ns:properties/ns:property",
                validXML,
                namespaceContext);

        Assert.assertEquals(
                2,
                elements.getLength());
    }

    @Test
    public void testEvaluateXPath_2() throws Exception {
        Document validXML = loadDocument("valid.xml");

        Node attribute = xmlUtilities.getFirstNodeForXPath(
                "ns:properties/ns:property/@name",
                validXML,
                namespaceContext);

        Assert.assertEquals(
                Node.ATTRIBUTE_NODE,
                attribute.getNodeType());

        Assert.assertEquals(
                "p1",
                ((Attr) attribute).getValue());
    }

}
