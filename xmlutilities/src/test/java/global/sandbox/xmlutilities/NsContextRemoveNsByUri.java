/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

public class NsContextRemoveNsByUri {

    @Test
    public void testRemoveByUri() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.addNamespace("p1", "http://global.sandbox/p1");

        Assert.assertEquals("p1", nsCtx.getPrefix("http://global.sandbox/p1"));
        nsCtx.removeNamespaceByURI("http://global.sandbox/p1");
        Assert.assertNull(nsCtx.getPrefix("http://global.sandbox/p1"));
    }

    @Test
    public void testRemoveMultipleByUri() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.addNamespace("p1", "http://global.sandbox/p");
        nsCtx.addNamespace("p2", "http://global.sandbox/p");

        List<String> expected = new ArrayList<String>(Arrays.asList("p1", "p2"));
        Iterator<String> prefixes = nsCtx.getPrefixes("http://global.sandbox/p");
        while (prefixes.hasNext()) {
            final String prefix = prefixes.next();
            Assert.assertTrue(expected.contains(prefix));
            expected.remove(prefix);
        }
        Assert.assertTrue(expected.isEmpty());

        nsCtx.removeNamespaceByURI("http://global.sandbox/p");

        Assert.assertFalse(nsCtx.getPrefixes("http://global.sandbox/p").hasNext());
    }

    @Test
    public void testIgnoreNullValue() {
        NamespaceContextImpl nsCtx = NamespaceContextImpl.emptyNamespaceContext();
        nsCtx.removeNamespaceByURI(null);
    }

}
