/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package global.sandbox.xmlutilities;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;


/**
 *
 * @author petr
 */
public class SuccessfullValidation extends XmlUtilitiesTestBase {

    private static final Logger LOGGER = Logger.getLogger(SuccessfullValidation.class.getName());

    @Test
    public void testValidDocument() throws Exception {
        Document schema = loadSchema();
        Document validXML = loadDocument("valid.xml");

        String result = xmlUtilities.validateXmlUsingSchema(
                XmlTools.documentToDomSource(validXML),
                XmlTools.documentToDomSource(schema));

        LOGGER.log(Level.INFO, "The resulting String is set to null if validation is OK: {0}", result);

        Assert.assertNull(result);
    }

}
