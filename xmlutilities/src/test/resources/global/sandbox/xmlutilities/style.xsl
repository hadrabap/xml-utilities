<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : style.xsl
    Created on : 18. prosinec 2007, 16:14
    Author     : petr
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns:ns="http://hadrabap.googlepages.com/projects/xmlutilities/sample.xsd"
                exclude-result-prefixes="ns">
    <xsl:strip-space elements="*" />

    <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes" />

    <xsl:template match="/">
        <html>
            <head>
                <title>style.xsl</title>
            </head>
            <body>
                <table>
                    <thead>
                        <tr>
                            <th>Parameter name</th>
                            <th>Parameter value</th>
                        </tr>
                    </thead>
                    <tbody><xsl:apply-templates /></tbody>
                </table>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="ns:property">
        <xsl:element name="tr">
            <xsl:element name="td">
                <xsl:value-of select="@name" />
            </xsl:element>
            <xsl:element name="td">
                <xsl:value-of select="." />
            </xsl:element>
        </xsl:element>
    </xsl:template>

</xsl:stylesheet>
