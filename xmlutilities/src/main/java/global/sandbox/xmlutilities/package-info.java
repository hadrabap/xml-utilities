/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

/**
 * Utilities and Tools that makes XML parsing and validation more easier.
 *
 * There are two ways to use this package:
 * <ul>
 *   <li>Static methods in <code>XmlTools</code> and</li>
 *   <li>Instance of <code>XmlUtilities</code> class.</li>
 * </ul>
 *
 * Static methods are good for simple usages. It shares one internal instance of XmlUtilities.
 *
 * Standalone XmlUtilities instance is good for multi-threading environment.
 *
 * All classes share on single exception which encapsulates all cases (checked exceptions) which might happen
 * in the processing based on its purpose.
 *
 * <hr>
 *
 * <h3>Functions, Methods and Tools</h3>
 *
 * <table border="1" cellpadding="5" cellspacing="0">
 *   <caption>Functions, Methods and Tools</caption>
 *   <thead>
 *     <tr>
 *       <th rowspan="2">Category</th>
 *       <th rowspan="2">Method</th>
 *       <th colspan="2">Availability</th>
 *     </tr>
 *     <tr>
 *       <th>in <code>XmlUtilities</code></th>
 *       <th>in <code>XmlTools</code></th>
 *     </tr>
 *   </thead>
 *   <tbody>
 *     <tr>
 *       <td rowspan="4">XML Loading</td>
 *       <td>from File to Document</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from Stream to Document</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from String to Document</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from String to Source</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td rowspan="3">Conversions</td>
 *       <td>Document to Source</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>Document to String</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>Source to String</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>Validation</td>
 *       <td>XML validation against XMLSchema</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td rowspan="2">XPath evaluation</td>
 *       <td>evaluate XPath that results to NodeList</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>evaluate XPath that results to single Node</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td rowspan="4">Transformation</td>
 *       <td>from Sources to Document</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from Templates and Source to Document</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from Sources to String</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *     <tr>
 *       <td>from Templates and Source to String</td>
 *       <td align="center">X</td>
 *       <td align="center">X</td>
 *     </tr>
 *   </tbody>
 *   <tfoot>
 *     <tr>
 *       <th colspan="4">Legend</th>
 *     </tr>
 *     <tr>
 *       <th>—</th>
 *       <td colspan="3"><em>Not available</em></td>
 *     </tr>
 *     <tr>
 *       <th>X</th>
 *       <td colspan="3"><em>Available</em></td>
 *     </tr>
 *   </tfoot>
 * </table>
 *
 * <hr>
 *
 * <h3>XML Loading</h3>
 *
 * The routines are defined in <code>XmlTools</code> and in <code>XmlUtilities</code>. The names of the methods starts
 * with <code>loadDocument</code> or <code>loadSource</code>.
 *
 * <br>
 *
 * The following example loads XML content from specified resource. The static method returns <code>Document</code> and
 * throws two Exceptions:
 * <dl>
 *   <dt><code>NoSuchResourceException</code></dt>
 *   <dd>if the resource does not exist</dd>
 *   <dt><code>XmlLoadingException</code></dt>
 *   <dd>this exception covers all "technical" exceptions</dd>
 * </dl>
 *
 * <a name="xmlloadingexample"></a>
 *
 * <pre style="border:1px solid black">
package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.transform.Source;
import org.w3c.dom.Document;

public class XmlLoading {

    public static Document loadXmlFromResourceAsDocument(Class&lt;?&gt; clazz, String name) throws NoSuchResourceException, XmlLoadingException {
        InputStream is = clazz.getResourceAsStream(name);

        if (is == null) {
            throw new NoSuchResourceException(name);
        }

<span style="background-color: rgb(255,255,200)">        try {
            return XmlTools.loadDocumentFromStream(is);
        } catch (XmlUtilitiesException ex) {
            throw new XmlLoadingException(ex);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                throw new XmlLoadingException(ex);
            }
        }</span>
    }

    public static class NoSuchResourceException extends Exception {

        private static final long serialVersionUID = 1L;

        public NoSuchResourceException(String resourceName) {
            super(resourceName);
        }

    }

    public static class XmlLoadingException extends Exception {

        private static final long serialVersionUID = 1L;

        public XmlLoadingException(Throwable cause) {
            super(cause);
        }

    }

}
 * </pre>
 *
 * <br>
 *
 * Starting from XML Utilities version 1.2 there are resource-loading-related methods available:
 *
 * <pre style="border:1px solid black">
...

    public static Document loadXmlFromResourceAsDocument12(Class&lt;?&gt; clazz, String name) throws XmlLoadingException {
        try {
            return XmlTools.loadDocumentFromResource(name, clazz);
        } catch (XmlUtilitiesException ex) {
            throw new XmlLoadingException(ex);
        }
    }

...
 * </pre>
 *
 * <h3>Conversions</h3>
 *
 * <code>XmlTools</code> allows you to convert <code>Document</code> to <code>Source</code> or <code>String</code>.
 *
 * <br>
 *
 * The following method is part of the <code>XmlLoading</code> class from the example
 * <a href="#xmlloadingexample">above</a>:
 *
 * <pre style="border: 1px solid black">
    public static Source loadXmlFromResourceAsSource(Class&lt;?&gt; clazz, String name) throws NoSuchResourceException, XmlLoadingException {
        return <span style="background-color: rgb(255,255,200)">XmlTools.documentToDomSource</span>((loadXmlFromResourceAsDocument(clazz, name));
    }
 * </pre>
 *
 * <h3>Validation</h3>
 *
 * Both, <code>XmlUtilities</code> and <code>XmlTools</code> allows you to validate specified XML file against specified
 * Schema. The following class shows you, how to use the static variant:
 *
 * <pre style="border: 1px solid black">
package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import javax.xml.transform.Source;
import org.w3c.dom.Document;

public class SchemaValidator {

    public static void validateXml(Source doc, Source schema) throws SchemaValidatorException, ValidationException {
        String result;

        try {
<span style="background-color: rgb(255,255,200)">            result = XmlTools.validateXmlUsingSchema(doc, schema);</span>
        } catch (XmlUtilitiesException ex) {
            throw new SchemaValidatorException(ex);
        }

        if (result != null) {
            throw new ValidationException(result);
        }
    }

    public static void validateXml(Document doc, Document schema) throws SchemaValidatorException, ValidationException {
        SchemaValidator.validateXml(XmlTools.documentToDomSource(doc), XmlTools.documentToDomSource(schema));
    }

    public static class SchemaValidatorException extends Exception {

        private static final long serialVersionUID = 1L;

        public SchemaValidatorException(Throwable cause) {
            super(cause);
        }

    }

    public static class ValidationException extends Exception {

        private static final long serialVersionUID = 1L;

        public ValidationException(String message) {
            super(message);
        }

    }

}
 * </pre>
 *
 * <h3>XPath Evaluation</h3>
 *
 * The following code shows how to easily evaluate XPath query. The method <code>loadAttribute</code> evaluates
 * specified query <em>(the query should end with <code>@xxx</code>)</em> and tries to convert obtained
 * <code>Node</code> to <code>Attr</code>. We can also try to cast <code>Node</code> to <code>Attr</code> and catch the
 * <code>ClassCastException</code>.
 *
 * <pre style="border: 1px solid black">
package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.NamespaceContextImpl;
import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import javax.xml.namespace.NamespaceContext;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XPathEvaluation {

    final static NamespaceContext NAMESPACES;

    static {
        final NamespaceContextImpl nspaces = NamespaceContextImpl.namespaceContextWithDefaults();
        nspaces.addNamespace(
                "ns",
                "http://hadrabap.googlepages.com/projects/xmlutilities/demo");

        NAMESPACES = nspaces;
    }

    public static Attr loadAttribute(Element rootNode, String xPathQuery) throws XPathEvaluationException {
        Node node;
        try {
<span style="background-color: rgb(255,255,200)">            node = XmlTools.getFirstNodeForXPath(
                    xPathQuery,
                    rootNode,
                    NAMESPACES);</span>
        } catch (XmlUtilitiesException ex) {
            throw new XPathEvaluationException(ex);
        }

        if (node == null) {
            return null;
        }

        if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
            return (Attr) node;
        } else {
            throw new XPathEvaluationException(String.format("Resulting node is not Attr, got %d.", node.getNodeType()));
        }
    }

    public static class XPathEvaluationException extends Exception {

        private static final long serialVersionUID = 1L;

        public XPathEvaluationException(String message) {
            super(message);
        }

        public XPathEvaluationException(Throwable cause) {
            super(cause);
        }

    }

}
 * </pre>
 *
 * <br>
 *
 * As of version 1.2, generics are supported. The above code might be simplified as follows:
 *
 * <pre style="border:1px solid black">
...

    public static Attr loadAttribute12(Element rootNode, String xPathQuery) throws XmlUtilitiesException {
        return XmlTools.getFirstNodeForXPath(
                xPathQuery,
                rootNode,
                NAMESPACES,
                Attr.class);
    }

...
 * </pre>
 *
 * <h3>Transformations</h3>
 *
 * <code>XmlUtilities</code> and <code>XmlTools</code> also simplify transformations. They support
 * <code>Source</code>-based and precompiled templates.
 *
 * <br>
 *
 * The following code demonstrates how to use the transformation support
 * in <code>XmlTools</code>:
 *
 * <pre style="border: 1px solid black">
package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import org.w3c.dom.Document;

public class Transformations {

    public static Document transform(Document doc, Document xsl) throws TransformationsException {
        try {
<span style="background-color: rgb(255,255,200)">            return XmlTools.transformToDocument(
                    XmlTools.documentToDomSource(xsl),
                    XmlTools.documentToDomSource(doc));</span>
        } catch (XmlUtilitiesException ex) {
            throw new TransformationsException(ex);
        }
    }

    public static class TransformationsException extends Exception {

        private static final long serialVersionUID = 1L;

        public TransformationsException(Throwable cause) {
            super(cause);
        }

    }

}
 * </pre>
 *
 * <hr>
 *
 * For more details please check the test cases.
 *
 * <hr>
 *
 * <h3>ChangeLog</h3>
 *
 * <table border="1" cellpadding="5" cellspacing="0">
 *   <caption>ChangeLog</caption>
 *   <thead>
 *     <tr>
 *       <th>Version</th>
 *       <th>Changes</th>
 *     </tr>
 *   </thead>
 *   <tbody>
 *     <tr>
 *       <td>1.0</td>
 *       <td>
 *         <ul>
 *           <li>Initial release</li>
 *         </ul>
 *       </td>
 *     </tr>
 *     <tr>
 *       <td>1.1</td>
 *       <td>
 *         <ul>
 *           <li>Added support for name spaces</li>
 *           <li>First public-domain release</li>
 *         </ul>
 *       </td>
 *     </tr>
 *     <tr>
 *       <td>1.2</td>
 *       <td>
 *         <ul>
 *           <li>Binary incompatible version</li>
 *           <li>Removed dependency on Xerces and Xalan-J; moved to JDK6</li>
 *           <li>Consolidated exception into single checked one</li>
 *           <li>Generics support for XPath evaluation</li>
 *           <li>Added support for output format properties</li>
 *           <li>Improved stream and string conversion functions</li>
 *           <li>Added stream and file writing functions</li>
 *           <li>Store functionality (streams, files)</li>
 *           <li>Added resource-loading functions</li>
 *         </ul>
 *       </td>
 *     </tr>
 *   </tbody>
 * </table>
 * <hr>
 */
package global.sandbox.xmlutilities;
