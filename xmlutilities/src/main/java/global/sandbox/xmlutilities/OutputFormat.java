/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import org.w3c.dom.Element;

/**
 * Convenient harness for configuration of {@link Transformer Transformers'} output intent.
 *
 * This class supports the following keys:
 * <ul>
 *  <li>{@code method}, {@link OutputKeys#METHOD}</li>
 *  <li>{@code version}, {@link OutputKeys#VERSION}</li>
 *  <li>{@code encoding}, {@link OutputKeys#ENCODING}</li>
 *  <li>{@code omit-xml-declaration}, {@link OutputKeys#OMIT_XML_DECLARATION}</li>
 *  <li>{@code standalone}, {@link OutputKeys#STANDALONE}</li>
 *  <li>{@code doctype-public}, {@link OutputKeys#DOCTYPE_PUBLIC}</li>
 *  <li>{@code doctype-system}, {@link OutputKeys#DOCTYPE_SYSTEM}</li>
 *  <li>{@code cdata-section-elements}, {@link OutputKeys#CDATA_SECTION_ELEMENTS}</li>
 *  <li>{@code indent}, {@link OutputKeys#INDENT}</li>
 *  <li>{@code media-type}, {@link OutputKeys#MEDIA_TYPE}</li>
 * </ul>
 *
 * @author Petr Hadraba
 *
 * @version 1.2
 */
public class OutputFormat implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Default version for XML output method.
     */
    public static final String XML_VERSION_1_0;

    /**
     * Default version for HTML output method.
     */
    public static final String HTML_VERSION_4_0;

    /**
     * List of supported keys by this class.
     */
    private static final List<String> SUPPORTED_KEYS;

    static {
        XML_VERSION_1_0 = "1.0";
        HTML_VERSION_4_0 = "4.0";

        final List<String> supportedKeys = new ArrayList<String>();
        supportedKeys.add(OutputKeys.METHOD);
        supportedKeys.add(OutputKeys.VERSION);
        supportedKeys.add(OutputKeys.ENCODING);
        supportedKeys.add(OutputKeys.OMIT_XML_DECLARATION);
        supportedKeys.add(OutputKeys.STANDALONE);
        supportedKeys.add(OutputKeys.DOCTYPE_PUBLIC);
        supportedKeys.add(OutputKeys.DOCTYPE_SYSTEM);
        supportedKeys.add(OutputKeys.CDATA_SECTION_ELEMENTS);
        supportedKeys.add(OutputKeys.INDENT);
        supportedKeys.add(OutputKeys.MEDIA_TYPE);
        SUPPORTED_KEYS = Collections.unmodifiableList(supportedKeys);
    }

    /**
     * output method.
     */
    private OutputMethod method;

    /**
     * version.
     */
    private String version;

    /**
     * encoding.
     */
    private String encoding;

    /**
     * omit XML Declaration flag.
     */
    private Boolean omitXmlDeclaration;

    /**
     * standalone flag.
     */
    private Boolean standalone;

    /**
     * DOCTYPE PUBLIC.
     */
    private String doctypePublic;

    /**
     * DOCTYPE SYSTEM.
     */
    private String doctypeSystem;

    /**
     * CDATA section element list.
     */
    private final List<String> cdataSectionElements = new ArrayList<String>();

    /**
     * Indent flag.
     */
    private Boolean indent;

    /**
     * Media type.
     */
    private String mediaType;

    /**
     * Custom properties.
     */
    private final Map<String, String> customProperties = new HashMap<String, String>();

    /**
     * Convenient interface for getting rendered codes.
     */
    private interface ConfigurationValue {

        /**
         * Returns code required by {@link Transformer#setOutputProperties(java.util.Properties) Transformer#setOutputProperties(java.util.Properties)}.
         *
         * @return text
         */
        String getValue();

    }

    /**
     * Typed representation of output method with appropriate and expected values by {@link Transformer Transformer}.
     *
     * @author Petr Hadraba
     *
     * @version 1.2
     */
    public enum OutputMethod implements ConfigurationValue {

        /**
         * XML output method.
         */
        XML("xml"),

        /**
         * HTML output method.
         */
        HTML("html"),

        /**
         * Plain text output method.
         */
        TEXT("text"),

        /**
         * Custom method.
         */
        CUSTOM(null);

        /**
         * Default code.
         */
        private final String defaultCode;

        /**
         * Used for custom value.
         */
        private String value;

        /**
         * Default constructor for default values.
         *
         * @param defaultCode code
         */
        OutputMethod(final String defaultCode) {
            this.defaultCode = defaultCode;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getValue() {
            if (this == CUSTOM) {
                if (value == null) {
                    throw new IllegalStateException(String.format("Missing value for type %s.", this.name()));
                }
                return value;
            }
            return defaultCode;
        }

        /**
         * Sets value for custom method.
         *
         * @param code code
         */
        public void setValue(final String code) {
            if (this != CUSTOM) {
                throw new IllegalStateException(String.format("Cannot set value for type %s.", this.name()));
            }
            if (code == null) {
                throw new IllegalArgumentException("The code cannot be null.");
            }
            this.value = code;
        }

    }

    /**
     * Represents Boolean value with appropriate value expected by {@link Transformer Transformer}.
     *
     * @author Petr Hadraba
     *
     * @version 1.2
     */
    public enum Boolean implements ConfigurationValue {

        /**
         * YES.
         */
        YES("yes"),

        /**
         * NO.
         */
        NO("no"),

        /**
         * YES.
         */
        TRUE("yes"),

        /**
         * NO.
         */
        FALSE("no");

        /**
         * Appropriate code.
         */
        private final String value;

        /**
         * Constructor for default values.
         *
         * @param value default code
         */
        Boolean(final String value) {
            this.value = value;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getValue() {
            return value;
        }

    }

    /**
     * Builder for declarative construction of {@link OutputFormat OutputFormat}.
     *
     * @author Petr Hadraba
     *
     * @version 1.2
     */
    public static final class Builder {

        /**
         * Under-layering output format.
         */
        private final OutputFormat outputFormat;

        /**
         * Creates new Builder with valid output method.
         *
         * @param method method
         * @param customCode custom code for {@link OutputMethod#CUSTOM CUSTOM}
         */
        private Builder(final OutputMethod method, final String customCode) {
            this.outputFormat = new OutputFormat();
            this.outputFormat.setMethod(method);
            if (method == OutputMethod.CUSTOM) {
                this.outputFormat.getMethod().setValue(customCode);
            }
        }

        /**
         * Returns under-layering Output Format object.
         *
         * @return output format
         */
        public OutputFormat build() {
            return outputFormat;
        }

        /**
         * Creates Properties ready to use in
         * {@link Transformer#setOutputProperties(java.util.Properties) setOutputProperties(java.util.Properties)}.
         *
         * @return Properties
         */
        public Properties buildToTransformerOutputProperties() {
            return outputFormat.toTransformerOutputProperties();
        }

        /**
         * Sets version.
         *
         * @param version text
         *
         * @return Builder
         */
        public Builder setVersion(final String version) {
            if (version == null) {
                throw new IllegalArgumentException("version can't be null.");
            }
            this.outputFormat.setVersion(version);
            return this;
        }

        /**
         * Sets default version for XML and HTML output method.
         *
         * @return Builder
         */
        public Builder withDefaultVersion() {
            switch (this.outputFormat.getMethod()) {
                case XML:
                    return setVersion(XML_VERSION_1_0);
                case HTML:
                    return setVersion(HTML_VERSION_4_0);
                default:
                    throw new IllegalStateException(String.format("Cannot set default version for method %s.", this.outputFormat.getMethod().name()));
            }
        }

        /**
         * Sets encoding.
         *
         * @param encoding text
         *
         * @return Builder
         */
        public Builder setEncoding(final String encoding) {
            if (encoding == null) {
                throw new IllegalArgumentException("encoding can't be null.");
            }
            this.outputFormat.setEncoding(encoding);
            return this;
        }

        /**
         * Sets encoding from specified Charset.
         *
         * @param charset Charset to use
         *
         * @return Builder
         */
        public Builder setEncoding(final Charset charset) {
            if (charset == null) {
                throw new IllegalArgumentException("charset can't be null.");
            }
            return setEncoding(charset.name());
        }

        /**
         * Sets omit XML Declaration flag.
         *
         * @param state new state
         *
         * @return Builder
         */
        public Builder setOmitXmlDeclaration(final Boolean state) {
            if (state == null) {
                throw new IllegalArgumentException("omitXmlDeclaration can't be null.");
            }
            this.outputFormat.setOmitXmlDeclaration(state);
            return this;
        }

        /**
         * Sets omit XML Declaration flag to {@code yes}.
         *
         * @return Builder
         */
        public Builder omitXmlDeclaration() {
            return setOmitXmlDeclaration(Boolean.YES);
        }

        /**
         * Sets omit XML Declaration flag to {@code no}.
         *
         * @return Builder
         */
        public Builder dontOmitXmlDeclaration() {
            return setOmitXmlDeclaration(Boolean.NO);
        }

        /**
         * Sets standalone flag.
         *
         * @param state new state
         *
         * @return Builder
         */
        public Builder setStandalone(final Boolean state) {
            if (state == null) {
                throw new IllegalArgumentException("standalone can't be null.");
            }
            this.outputFormat.setStandalone(state);
            return this;
        }

        /**
         * Sets standalone flag.
         *
         * @return Builder
         */
        public Builder asStandalone() {
            return setStandalone(Boolean.YES);
        }

        /**
         * Sets DOCTYPE PUBLIC
         *
         * @param doctype DOCTYPE PUBLIC
         *
         * @return Builder
         */
        public Builder setDoctypePublic(final String doctype) {
            if (doctype == null) {
                throw new IllegalArgumentException("doctype PUBLIC can't be null.");
            }
            this.outputFormat.setDoctypePublic(doctype);
            return this;
        }

        /**
         * Sets DOCTYPE SYSTEM.
         *
         * @param doctype DOCTYPE SYSTEM
         *
         * @return Builder
         */
        public Builder setDoctypeSystem(final String doctype) {
            if (doctype == null) {
                throw new IllegalArgumentException("doctype SYSTEM can't be null.");
            }
            this.outputFormat.setDoctypeSystem(doctype);
            return this;
        }

        /**
         * Adds element name as is into CDATA section element list.
         *
         * @param elementName name to add
         *
         * @return Builder
         */
        public Builder addCdataSectionElement(final String elementName) {
            if (elementName == null) {
                throw new IllegalArgumentException("elementName can't be null.");
            }
            this.outputFormat.addCdataSectionElement(elementName);
            return this;
        }

        /**
         * Adds specified local name with optional name space URI into CDATA section element list.
         *
         * @param namespaceUri optional name space URI
         * @param localName element name
         *
         * @return Builder
         */
        public Builder addCdataSectionElement(final String namespaceUri, String localName) {
            if (localName == null) {
                throw new IllegalArgumentException("localName can't be null.");
            }
            if (namespaceUri == null) {
                return addCdataSectionElement(localName);
            } else {
                return addCdataSectionElement(String.format("{%s}%s", namespaceUri, localName));
            }
        }

        /**
         * Adds specified {@link Element Element} into CDATA section element list. The element is resolved to
         * qualified name.
         *
         * @param element element to add
         *
         * @return Builder
         */
        public Builder addCdataSectionElement(final Element element) {
            return addCdataSectionElement(element.getNamespaceURI(), element.getLocalName());
        }

        /**
         * Sets indent flag.
         *
         * @param state state
         *
         * @return Builder
         */
        public Builder setIndent(final Boolean state) {
            if (state == null) {
                throw new IllegalArgumentException("state can't be null.");
            }
            this.outputFormat.setIndent(state);
            return this;
        }

        /**
         * Sets indent flag.
         *
         * @return Builder
         */
        public Builder indent() {
            return setIndent(Boolean.YES);
        }

        /**
         * Sets media type.
         *
         * @param mediaType media type
         *
         * @return Builder
         */
        public Builder setMediaType(final String mediaType) {
            if (mediaType == null) {
                throw new IllegalArgumentException("mediaType can't be null.");
            }
            this.outputFormat.setMediaType(mediaType);
            return this;
        }

        /**
         * Adds custom property which does not exist already and is not one of the properties managed by this builder.
         *
         * @param key key
         * @param value value
         *
         * @return Builder
         */
        public Builder withCustomProperty(final String key, final String value) {
            if (key == null) {
                throw new IllegalArgumentException("key can't be null.");
            }
            if (value == null) {
                throw new IllegalArgumentException("value can't be null.");
            }
            if (SUPPORTED_KEYS.contains(key)) {
                throw new IllegalArgumentException(String.format("The key '%s' is directly controlled by Builder.", key));
            }
            this.outputFormat.addCustomProperty(key, value);
            return this;
        }

    }

    /**
     * Creates builder for XML method intent.
     *
     * @return Builder
     */
    public static Builder newXmlMethodBuilder() {
        return new Builder(OutputMethod.XML, null);
    }

    /**
     * Creates builder for HTML method intent.
     *
     * @return Builder
     */
    public static Builder newHtmlMethodBuilder() {
        return new Builder(OutputMethod.HTML, null);
    }

    /**
     * Creates builder for Text method intent.
     *
     * @return Builder
     */
    public static Builder newTextMethodBuilder() {
        return new Builder(OutputMethod.TEXT, null);
    }

    /**
     * Creates builder for custom method.
     *
     * @param method mandatory name of the custom method
     *
     * @return Builder
     */
    public static Builder newCustomMethodBuilder(String method) {
        return new Builder(OutputMethod.CUSTOM, method);
    }

    /**
     * Renders {@code this} class into {@link Properties Properties} ready to be used by
     * {@link Transformer#setOutputProperties(java.util.Properties) setOutputProperties(java.util.Properties)}.
     *
     * @return Properties
     */
    public Properties toTransformerOutputProperties() {
        Properties result = new Properties();

        addPropertyIfDefined(result, OutputKeys.METHOD, method);
        addPropertyIfDefined(result, OutputKeys.VERSION, version);
        addPropertyIfDefined(result, OutputKeys.ENCODING, encoding);
        addPropertyIfDefined(result, OutputKeys.OMIT_XML_DECLARATION, omitXmlDeclaration);
        addPropertyIfDefined(result, OutputKeys.STANDALONE, standalone);
        addPropertyIfDefined(result, OutputKeys.DOCTYPE_PUBLIC, doctypePublic);
        addPropertyIfDefined(result, OutputKeys.DOCTYPE_SYSTEM, doctypeSystem);
        addPropertyIfDefined(result, OutputKeys.INDENT, indent);
        addPropertyIfDefined(result, OutputKeys.MEDIA_TYPE, mediaType);
        addPropertyIfDefined(result, OutputKeys.CDATA_SECTION_ELEMENTS, renderCdataSectionElementList(cdataSectionElements));
        result.putAll(customProperties);

        return result;
    }

    /**
     * Adds property if value is not {@code null}.
     *
     * @param result target map
     * @param key key
     * @param value value
     */
    private static void addPropertyIfDefined(final Properties result, final String key, final String value) {
        if (value != null) {
            result.put(key, value);
        }
    }

    /**
     * Adds property if value is not {@code null}.
     *
     * @param result target map
     * @param key key
     * @param value value
     */
    private static void addPropertyIfDefined(final Properties result, final String key, final ConfigurationValue value) {
        if (value != null) {
            result.put(key, value.getValue());
        }
    }

    /**
     * Renders list of CDATA section elements into property value text.
     *
     * @param elements list of CDATA section elements
     *
     * @return text
     */
    private static String renderCdataSectionElementList(final List<String> elements) {
        if (elements.isEmpty()) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        for (String element : elements) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(element);
        }
        return sb.toString();
    }

    /**
     * Sets output method.
     *
     * @param method method
     */
    public void setMethod(OutputMethod method) {
        this.method = method;
    }

    /**
     * Sets version.
     *
     * @param version version string
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Sets encoding.
     *
     * @param encoding encoding name
     */
    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    /**
     * Sets omit XML declaration flag.
     *
     * @param omitXmlDeclaration state
     */
    public void setOmitXmlDeclaration(Boolean omitXmlDeclaration) {
        this.omitXmlDeclaration = omitXmlDeclaration;
    }

    /**
     * Sets standalone flag.
     *
     * @param standalone state
     */
    public void setStandalone(Boolean standalone) {
        this.standalone = standalone;
    }

    /**
     * Sets DOCTYPE PUBLIC.
     *
     * @param doctypePublic value
     */
    public void setDoctypePublic(String doctypePublic) {
        this.doctypePublic = doctypePublic;
    }

    /**
     * Sets DOCTYPE SYSTEM.
     *
     * @param doctypeSystem value
     */
    public void setDoctypeSystem(String doctypeSystem) {
        this.doctypeSystem = doctypeSystem;
    }

    /**
     * Sets indent flag.
     *
     * @param indent state
     */
    public void setIndent(Boolean indent) {
        this.indent = indent;
    }

    /**
     * Sets media type.
     *
     * @param mediaType media type
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * Adds qualified element name into CDATA section element list. The element should not exist in the list already.
     *
     * @param elementName element to add
     */
    public void addCdataSectionElement(String elementName) {
        if (this.cdataSectionElements.contains(elementName)) {
            throw new IllegalArgumentException(String.format("The element '%s' already specified.", elementName));
        }
        this.cdataSectionElements.add(elementName);
    }

    /**
     * Adds specified key-value property. The property should not be one of the properties directly supported by
     * this class.
     *
     * @param key key
     * @param value value
     */
    public void addCustomProperty(final String key, final String value) {
        if (customProperties.containsKey(key)) {
            throw new IllegalArgumentException(String.format("The key '%s' is already defined.", key));
        }
        this.customProperties.put(key, value);
    }

    /**
     * Returns output method.
     *
     * @return output method
     */
    public OutputMethod getMethod() {
        return method;
    }

    /**
     * Returns version.
     *
     * @return version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Returns encoding.
     *
     * @return encoding
     */
    public String getEncoding() {
        return encoding;
    }

    /**
     * Returns omit XML declaration flag.
     *
     * @return omit XML declaration flag
     */
    public Boolean getOmitXmlDeclaration() {
        return omitXmlDeclaration;
    }

    /**
     * Returns standalone.
     *
     * @return standalone
     */
    public Boolean getStandalone() {
        return standalone;
    }

    /**
     * Returns DOCTYPE PUBLIC.
     *
     * @return DOCTYPE PUBLIC
     */
    public String getDoctypePublic() {
        return doctypePublic;
    }

    /**
     * Returns DOCTYPE SYSTEM.
     *
     * @return DOCTYPE SYSTEM
     */
    public String getDoctypeSystem() {
        return doctypeSystem;
    }

    /**
     * Returns list of CDATA section elements.
     *
     * @return list of CDATA section elements
     */
    public List<String> getCdataSectionElements() {
        return cdataSectionElements;
    }

    /**
     * Returns indent.
     *
     * @return indent
     */
    public Boolean getIndent() {
        return indent;
    }

    /**
     * Returns media type.
     *
     * @return media type
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * Returns custom properties.
     *
     * @return custom properties
     */
    public Map<String, String> getCustomProperties() {
        return customProperties;
    }

}
