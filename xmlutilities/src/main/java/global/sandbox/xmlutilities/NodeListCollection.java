/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * {@link ArrayList ArrayList} like implementation for {@link NodeList NodeList} which is unmodifiable, not serializable
 * and exposes access to original {@link NodeList NodeList}. The under-layering implementation is based on a copy,
 * modifications made to the original {@link NodeList NodeList} have no effect to iterators.
 *
 * The name of this class has been chosen in respect to possible collision with {@link NodeList org.w3c.dom.NodeList}
 * which may occur during import.
 *
 * @param <E> element type
 *
 * @author Petr Hadraba
 *
 * @version 1.2
 */
public class NodeListCollection<E extends Node> implements List<E> {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XmlUtilities.XMLUTILITIES_LOGGER_NAME);

    /**
     * Class type of elements.
     */
    private final Class<E> elementType;

    /**
     * Original {@link NodeList NodeList}.
     */
    private final NodeList nodeList;

    /**
     * Unmodifiable implementation.
     */
    private final List<E> nodes;

    /**
     * Creates new instance from {@link NodeList NodeList}. The under-layering implementation is constant, read only,
     * unmodifiable. The implementation is intentionally not serializable.
     *
     * @param nodeList under-layering {@link NodeList NodeList}
     * @param elementType class type of element
     *
     * @throws XmlUtilitiesException on error (casting, data type conversion)
     */
    @SuppressWarnings("unchecked")
    public NodeListCollection(NodeList nodeList, Class<E> elementType) throws XmlUtilitiesException {
        this.elementType = elementType;
        this.nodeList = nodeList;

        if (nodeList != null && nodeList.getLength() > 0) {
            List<E> _nodes = new ArrayList<E>();
            for (int i = 0; i < nodeList.getLength(); i++) {
                final Node node = nodeList.item(i);
                final E element;
                try {
                    element = elementType.cast(node);
                } catch (ClassCastException ex) {
                    final String message = String.format("Failed to cast element at index %d from %s to %s.", i, node.getClass(), elementType);
                    LOGGER.log(Level.SEVERE, message, ex);
                    throw new XmlUtilitiesException(message, ex);
                }
                _nodes.add(element);
            }
            this.nodes = Collections.unmodifiableList(_nodes);
        } else {
            this.nodes = Collections.unmodifiableList(Collections.EMPTY_LIST);
        }
    }

    /**
     * Returns class type of element.
     *
     * @return class type of element
     */
    public Class<E> getElementType() {
        return elementType;
    }

    /**
     * Returns under-layering {@link NodeList NodeList}.
     *
     * @return under-layering {@link NodeList NodeList}
     */
    public NodeList getNodeList() {
        return nodeList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int size() {
        return nodes.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isEmpty() {
        return nodes.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean contains(Object o) {
        return nodes.contains(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Iterator<E> iterator() {
        return nodes.iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] toArray() {
        return nodes.toArray();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T[] toArray(T[] a) {
        return nodes.<T>toArray(a);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean add(E e) {
        return nodes.add(e);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(Object o) {
        return nodes.remove(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean containsAll(Collection<?> c) {
        return nodes.containsAll(c);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addAll(Collection<? extends E> c) {
        return nodes.addAll(c);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return nodes.addAll(index, c);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeAll(Collection<?> c) {
        return nodes.removeAll(c);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean retainAll(Collection<?> c) {
        return nodes.retainAll(c);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clear() {
        nodes.clear();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E get(int index) {
        return nodes.get(index);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E set(int index, E element) {
        return nodes.set(index, element);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void add(int index, E element) {
        nodes.add(index, element);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public E remove(int index) {
        return nodes.remove(index);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int indexOf(Object o) {
        return nodes.indexOf(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int lastIndexOf(Object o) {
        return nodes.lastIndexOf(o);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ListIterator<E> listIterator() {
        return nodes.listIterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ListIterator<E> listIterator(int index) {
        return nodes.listIterator(index);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return nodes.subList(fromIndex, toIndex);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        return obj == this || nodes.equals(obj);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return nodes.hashCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return nodes.toString();
    }

}
