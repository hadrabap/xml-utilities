/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Common exception related to XML Utilities.
 *
 * @author Petr Hadraba
 *
 * @since 1.2
 */
public class XmlUtilitiesException extends Exception implements Iterable<Throwable> {

    private static final long serialVersionUID = 1L;

    /**
     * Consequent exception; usually finally block.
     */
    private final List<Throwable> consequents = new CopyOnWriteArrayList<Throwable>();

    public XmlUtilitiesException() {
    }

    public XmlUtilitiesException(String message) {
        super(message);
    }

    public XmlUtilitiesException(Throwable cause) {
        super(cause);
    }

    public XmlUtilitiesException(String message, Throwable cause) {
        super(message, cause);
    }

    private static final Throwable[] EMPTY_THROWABLE_ARRAY = new Throwable[] {};

    public Throwable[] getConsequent() {
        return consequents.toArray(EMPTY_THROWABLE_ARRAY);
    }

    public void addConsequent(Throwable consequent) {
        this.consequents.add(consequent);
    }

    private static final String INTERNAL_ENCODING = "UTF-8";

    private static final Charset INTERNAL_CHARSET;

    static {
        INTERNAL_CHARSET = Charset.forName(INTERNAL_ENCODING);
    }

    private static final String CONSEQUENT_CAPTION = "By consequent: ";

    @Override
    public void printStackTrace(PrintStream s) {
        printStackTrace(new WrappedPrintStream(s));
    }

    @Override
    public void printStackTrace(PrintWriter s) {
        printStackTrace(new WrappedPrintWriter(s));
    }

    @Override
    public void printStackTrace() {
        printStackTrace(System.err);
    }

    @Override
    public Iterator<Throwable> iterator() {
        return consequents.iterator();
    }

    private void printStackTrace(PrintStreamOrWriter s) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(baos, INTERNAL_CHARSET);
        PrintWriter pw = new PrintWriter(osw);
        super.printStackTrace(pw);
        pw.flush();
        s.print(new String(baos.toByteArray(), INTERNAL_CHARSET));

        for (Throwable consequent : consequents) {
            s.print(CONSEQUENT_CAPTION);
            s.printStackTrace(consequent);
        }
        s.flush();
    }

    private static interface PrintStreamOrWriter {

        abstract void print(String text);

        abstract void println(String text);

        abstract void flush();

        abstract void printStackTrace(Throwable t);

    }

    private static class WrappedPrintWriter implements PrintStreamOrWriter {

        private final PrintWriter printWriter;

        WrappedPrintWriter(PrintWriter printWriter) {
            this.printWriter = printWriter;
        }

        @Override
        public void print(String text) {
            printWriter.print(text);
        }

        @Override
        public void println(String text) {
            printWriter.println(text);
        }

        @Override
        public void flush() {
            printWriter.flush();
        }

        @Override
        public void printStackTrace(Throwable t) {
            t.printStackTrace(printWriter);
        }

    }

    private static class WrappedPrintStream implements PrintStreamOrWriter {

        private final PrintStream printStream;

        WrappedPrintStream(PrintStream printStream) {
            this.printStream = printStream;
        }

        @Override
        public void print(String text) {
            printStream.print(text);
        }

        @Override
        public void println(String text) {
            printStream.println(text);
        }

        @Override
        public void flush() {
            printStream.flush();
        }

        @Override
        public void printStackTrace(Throwable t) {
            t.printStackTrace(printStream);
        }

    }

}
