/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This class provides simple XML utilities.
 *
 * @author Petr Hadraba
 *
 * @version 1.2
 */
public class XmlUtilities {

    /**
     * Name of internal logger.
     */
    static final String XMLUTILITIES_LOGGER_NAME;

    static {
        XMLUTILITIES_LOGGER_NAME = XmlUtilities.class.getName();
    }

    /**
     * Internal logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XMLUTILITIES_LOGGER_NAME);

    /**
     * Stores schema factory.
     */
    private final SchemaFactory schemaFactory;

    /**
     * Stores document builder factory.
     */
    private final DocumentBuilderFactory documentBuilderFactory;

    /**
     * Stores document builder.
     */
    private final DocumentBuilder documentBuilder;

    /**
     * Stores localTransformer factory.
     */
    private final TransformerFactory transformerFactory;

    /**
     * Stores localTransformer.
     */
    private final Transformer transformer;

    /**
     * Stores XPath factory.
     */
    private final XPathFactory xpathFactory;

    /**
     * Stores XPath evaluator.
     */
    private final XPath xpath;

    /**
     * Stores the default NamespaceContext.
     */
    private final NamespaceContext defaultNamespaceContext;

    /**
     * Initializes XML Utilities.
     *
     * Creates new factories and new default instances.
     *
     * @throws XmlUtilitiesException on error
     */
    public XmlUtilities() throws XmlUtilitiesException {
        this(null, null, null, null, null, null, null, null);
    }

    /**
     * Initializes XML Utilities.
     *
     * Creates new factories and new instances according to custom objects.
     *
     * @param documentBuilderFactory custom DocumentBuilderFactory
     * @param documentBuilder custom DocumentBuilder
     * @param schemaFactory custom SchemaFactory
     * @param transformerFactory custom TransformerFactory
     * @param transformer custom Transformer
     * @param xpathFactory custom XPathFactory
     * @param xpath custom XPath
     * @param defaultNamespaceContext custom name spaces
     *
     * @throws XmlUtilitiesException on error
     */
    public XmlUtilities(
            final DocumentBuilderFactory documentBuilderFactory,
            final DocumentBuilder documentBuilder,
            final SchemaFactory schemaFactory,
            final TransformerFactory transformerFactory,
            final Transformer transformer,
            final XPathFactory xpathFactory,
            final XPath xpath,
            final NamespaceContext defaultNamespaceContext)
            throws XmlUtilitiesException {
        try {
            if (documentBuilderFactory == null) {
                LOGGER.log(Level.FINEST, "Using default Name Space aware DocumentBuilderFactory.");
                this.documentBuilderFactory = DocumentBuilderFactory.newInstance();
                this.documentBuilderFactory.setNamespaceAware(true);
            } else {
                LOGGER.log(Level.FINEST, "Using provided DocumentBuilderFactory.");
                this.documentBuilderFactory = documentBuilderFactory;
            }

            if (documentBuilder == null) {
                LOGGER.log(Level.FINEST, "Using default DocumentBuilder.");
                this.documentBuilder = this.documentBuilderFactory.newDocumentBuilder();
            } else {
                LOGGER.log(Level.FINEST, "Using provided DocumentBuilder.");
                this.documentBuilder = documentBuilder;
            }

            if (schemaFactory == null) {
                LOGGER.log(Level.FINEST, "Using default SchemaFactory.");
                this.schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            } else {
                LOGGER.log(Level.FINEST, "Using provided SchemaFactory.");
                this.schemaFactory = schemaFactory;
            }

            if (transformerFactory == null) {
                LOGGER.log(Level.FINEST, "Using default TransformerFactory.");
                this.transformerFactory = TransformerFactory.newInstance();
            } else {
                LOGGER.log(Level.FINEST, "Using provided TransformerFactory.");
                this.transformerFactory = transformerFactory;
            }

            if (transformer == null) {
                LOGGER.log(Level.FINEST, "Using default Transformer.");
                this.transformer = this.transformerFactory.newTransformer();
            } else {
                LOGGER.log(Level.FINEST, "Using provided Transformer.");
                this.transformer = transformer;
            }

            if (xpathFactory == null) {
                LOGGER.log(Level.FINEST, "Using default XPathFactory.");
                this.xpathFactory = XPathFactory.newInstance(XPathFactory.DEFAULT_OBJECT_MODEL_URI);
            } else {
                LOGGER.log(Level.FINEST, "Using provided XPathFactory.");
                this.xpathFactory = xpathFactory;
            }

            if (xpath == null) {
                LOGGER.log(Level.FINEST, "Using default XPath.");
                this.xpath = this.xpathFactory.newXPath();
            } else {
                LOGGER.log(Level.FINEST, "Using provided XPath.");
                this.xpath = xpath;
            }

            if (defaultNamespaceContext == null) {
                LOGGER.log(Level.FINEST, "Using default NamespaceContextImpl.");
                this.defaultNamespaceContext = NamespaceContextImpl.namespaceContextWithDefaults();
            } else {
                LOGGER.log(Level.FINEST, "Using provided NamespaceContext.");
                this.defaultNamespaceContext = defaultNamespaceContext;
            }
        } catch (ParserConfigurationException ex) {
            final String message = "Failed to initialize XML Utilities.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (TransformerFactoryConfigurationError ex) {
            final String message = "Failed to initialize XML Utilities.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (TransformerConfigurationException ex) {
            final String message = "Failed to initialize XML Utilities.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (XPathFactoryConfigurationException ex) {
            final String message = "Failed to initialize XML Utilities.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
    }

    /**
     * Returns SchemaFactory object internally used by the XML Utilities.
     *
     * @return SchemaFactory object
     */
    public SchemaFactory getSchemaFactory() {
        return schemaFactory;
    }

    /**
     * Returns DocumentBuilderFactory object internally used by the XML Utilities.
     *
     * @return DocumentBuilderFactory object
     */
    public DocumentBuilderFactory getDocumentBuilderFactory() {
        return documentBuilderFactory;
    }

    /**
     * Returns DocumentBuilder object internally used by the XML Utilities.
     *
     * @return DocumentBuilder object
     */
    public DocumentBuilder getDocumentBuilder() {
        return documentBuilder;
    }

    /**
     * Returns TransformerFactory object internally used by the XML Utilities.
     *
     * @return TransformerFactory object
     */
    public TransformerFactory getTransformerFactory() {
        return transformerFactory;
    }

    /**
     * Returns Transformer object internally used by the XML Utilities.
     *
     * @return Transformer object
     */
    public Transformer getTransformer() {
        return transformer;
    }

    /**
     * Returns XPathFactory object internally used by the XML Utilities.
     *
     * @return XPathFactory object
     */
    public XPathFactory getXPathFactory() {
        return xpathFactory;
    }

    /**
     * Returns XPath object internally used by the XML Utilities.
     *
     * @return XPath object
     */
    public XPath getXPath() {
        return xpath;
    }

    /**
     * Returns default NamespaceContext used by the XML Utilities.
     *
     * @return default NamespaceContenxt object
     */
    public NamespaceContext getDefaultNamespaceContext() {
        return defaultNamespaceContext;
    }

    /**
     * Validates XML Document against XML Schema.
     *
     * @param xmlDocument document to validate
     * @param xmlSchema XMLSchema
     *
     * @return {@code null} if successful, string with error otherwise
     *
     * @throws XmlUtilitiesException on error
     */
    public String validateXmlUsingSchema(final Source xmlDocument, final Source xmlSchema) throws XmlUtilitiesException {
        final Validator validator;

        try {
            validator = schemaFactory.newSchema(xmlSchema).newValidator();

            validator.validate(xmlDocument);
        } catch (SAXException ex) {
            return ex.getMessage();
        } catch (IOException ex) {
            final String message = "Failed to validate XML Document against XML Schema.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }

        return null;
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromFile(final String fileName) throws XmlUtilitiesException {
        return loadDocumentFromFile(new File(fileName));
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     * @param documentBuilder custom document builder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromFile(final String fileName, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        return loadDocumentFromFile(new File(fileName), documentBuilder);
    }

    /**
     * Loads XML from file specified with the File object.
     *
     * @param file file to load
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromFile(final File file) throws XmlUtilitiesException {
        return loadDocumentFromFile(file, documentBuilder);
    }

    /**
     * Loads XML from file specified with the file object.
     *
     * @param file file to load
     * @param documentBuilder custom document builder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromFile(final File file, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        final InputStream inputStream;
        try {
            inputStream = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            final String message = String.format("The specified file '%s' does not exist.", file);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }

        final Document document = loadDocumentFromStreamAndClose(inputStream, documentBuilder, true);

        return document;
    }

    /**
     * loads XML from specified input stream. The stream is not being closed.
     *
     * @param is input stream
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromStream(final InputStream is) throws XmlUtilitiesException {
        return loadDocumentFromStream(is, documentBuilder);
    }

    /**
     * Loads XML from specified input stream. The stream is not being closed.
     *
     * @param is input stream
     * @param documentBuilder custom Document Builder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromStream(final InputStream is, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(is);

        try {
            final Document document = documentBuilder.parse(bufferedInputStream);

            return document;
        } catch (IOException ex) {
            final String message = "Failed to read data from stream.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (SAXException ex) {
            final String message = "Failed to parse XML from stream.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
    }

    /**
     * Loads XML from specified input stream and closes it if specified.
     *
     * @param is input stream
     * @param close {@code true} to close input stream, {@code false} to leave it unclosed
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromStreamAndClose(final InputStream is, final boolean close) throws XmlUtilitiesException {
        return loadDocumentFromStreamAndClose(is, documentBuilder, close);
    }

    /**
     * Loads XML from specified input stream and closes it if specified.
     *
     * @param is input stream
     * @param documentBuilder custom Document Builder
     * @param close {@code true} to close input stream, {@code false} to leave it unclosed
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromStreamAndClose(final InputStream is, final DocumentBuilder documentBuilder, final boolean close) throws XmlUtilitiesException {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(is);

        Document document = null;

        XmlUtilitiesException exception = null;
        try {
            try {
                document = documentBuilder.parse(bufferedInputStream);
            } catch (IOException ex) {
                final String message = "Failed to read data from stream.";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            } catch (SAXException ex) {
                final String message = "Failed to parse XML from stream.";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            }
        } finally {
            if (close) {
                try {
                    bufferedInputStream.close();
                } catch (IOException ex) {
                    final String message = "Failed to close stream.";
                    if (exception != null) {
                        LOGGER.log(Level.WARNING, message, ex);
                        exception.addConsequent(ex);
                    } else {
                        LOGGER.log(Level.SEVERE, message, ex);
                        exception = new XmlUtilitiesException(message, ex);
                    }
                }
            }

            if (exception != null) {
                throw exception;
            }
        }

        return document;
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromClassLoader(final String resource, final Class<?> fallbackClazz) throws XmlUtilitiesException {
        return loadDocumentFromClassLoader(resource, documentBuilder, fallbackClazz);
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param documentBuilder custom DocumentBuilder
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromClassLoader(final String resource, final DocumentBuilder documentBuilder, final Class<?> fallbackClazz) throws XmlUtilitiesException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        if (classLoader == null) {
            if (fallbackClazz != null) {
                LOGGER.log(Level.FINEST, "ContextClassLoader is null.");
                LOGGER.log(Level.FINEST, "About to use ClassLoader from provided fallback Class.");
                classLoader = fallbackClazz.getClassLoader();
            }
        }

        if (classLoader == null) {
            classLoader = ClassLoader.getSystemClassLoader();
            if (classLoader != null) {
                LOGGER.log(Level.FINEST, "About to use SystemClassLoader.");
            }
        }

        if (classLoader == null) {
            final String message = "Failed to retrieve ContextClassLoader, ClassLoader or SystemClassLoader.";
            LOGGER.log(Level.SEVERE, message);
            throw new XmlUtilitiesException(message);
        }

        InputStream is = classLoader.getResourceAsStream(resource);

        if (is == null) {
            final String message = String.format("The specified resource '%s' has not been found using ClassLoader %s.", resource, classLoader.toString());
            LOGGER.log(Level.SEVERE, message);
            throw new XmlUtilitiesException(message);
        }

        return loadDocumentFromStreamAndClose(is, documentBuilder, true);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param clazz class to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromResource(final String resource, final Class<?> clazz) throws XmlUtilitiesException {
        return loadDocumentFromResource(resource, documentBuilder, clazz);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param clazz class to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final Class<?> clazz) throws XmlUtilitiesException {
        InputStream is = clazz.getResourceAsStream(resource);

        if (is == null) {
            final String message = String.format("The resource '%s' not found.", resource);
            LOGGER.log(Level.SEVERE, message);
            throw new XmlUtilitiesException(message);
        }

        return loadDocumentFromStreamAndClose(is, documentBuilder, true);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param classLoader ClassLoader to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromResource(final String resource, final ClassLoader classLoader) throws XmlUtilitiesException {
        return loadDocumentFromResource(resource, documentBuilder, classLoader);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param classLoader ClassLoader to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final ClassLoader classLoader) throws XmlUtilitiesException {
        InputStream is = classLoader.getResourceAsStream(resource);

        if (is == null) {
            final String message = String.format("The resource '%s' not found.", resource);
            LOGGER.log(Level.SEVERE, message);
            throw new XmlUtilitiesException(message);
        }

        return loadDocumentFromStreamAndClose(is, documentBuilder, true);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromString(final String source) throws XmlUtilitiesException {
        return loadDocumentFromString(source, documentBuilder);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     * @param documentBuilder custom document builder
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public Document loadDocumentFromString(final String source, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        final StringReader sr = new StringReader(source);
        final BufferedReader br = new BufferedReader(sr);
        final InputSource is = new InputSource(br);

        Document result = null;

        XmlUtilitiesException exception = null;
        try {
            try {
                result = documentBuilder.parse(is);
            } catch (SAXException ex) {
                final String message = "Failed to parse document from String.";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            } catch (IOException ex) {
                final String message = "Failed to parse document from String (I/O).";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            }
        } finally {
            try {
                br.close();
                sr.close();
            } catch (IOException ex) {
                final String message = "Failed to close().";
                if (exception != null) {
                    LOGGER.log(Level.WARNING, message, ex);
                    exception.addConsequent(ex);
                } else {
                    LOGGER.log(Level.SEVERE, message, ex);
                    exception = new XmlUtilitiesException(message, ex);
                }
            }

            if (exception != null) {
                throw exception;
            }
        }

        return result;
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String documentToString(final Document source) throws XmlUtilitiesException {
        return sourceToString(XmlTools.documentToDomSource(source));
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputFormat output format configuration
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String documentToString(final Document source, final OutputFormat outputFormat) throws XmlUtilitiesException {
        return sourceToString(XmlTools.documentToDomSource(source), outputFormat);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String documentToString(final Document source, final Properties outputProperties) throws XmlUtilitiesException {
        return sourceToString(XmlTools.documentToDomSource(source), outputProperties);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source Document to convert
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String sourceToString(final Source source) throws XmlUtilitiesException {
        return sourceToString(source, (Properties) null);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source Document to convert
     * @param outputFormat output format configuration
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String sourceToString(final Source source, final OutputFormat outputFormat) throws XmlUtilitiesException {
        Properties outputProperties = null;
        if (outputFormat != null) {
            outputProperties = outputFormat.toTransformerOutputProperties();
        }
        return sourceToString(source, outputProperties);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source Document to convert
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String sourceToString(final Source source, final Properties outputProperties) throws XmlUtilitiesException {
        final StringWriter stringWriter = new StringWriter();
        final StreamResult streamResult = new StreamResult(stringWriter);

        transformer.setParameter("", "");
        transformer.clearParameters();
        transformer.setOutputProperties(null);
        if (outputProperties != null) {
            transformer.setOutputProperties(outputProperties);
        }
        try {
            transformer.transform(source, streamResult);
        } catch (TransformerException ex) {
            final String message = "Failed to convert Source to String.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }

        final String result = stringWriter.toString();

        try {
            stringWriter.close();
        } catch (IOException ex) {
            final String message = "Failed to close() StringWriter.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }

        return result;
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public NodeList evaluateXPath(final String query, final Node context) throws XmlUtilitiesException {
        return evaluateXPath(query, context, (NamespaceContext) null);
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public NodeList evaluateXPath(final String query, final Node context, final NamespaceContext namespaces) throws XmlUtilitiesException {
        xpath.reset();

        if (namespaces == null) {
            xpath.setNamespaceContext(defaultNamespaceContext);
        } else {
            xpath.setNamespaceContext(namespaces);
        }

        try {
            return (NodeList) xpath.evaluate(query, context, XPathConstants.NODESET);
        } catch (XPathExpressionException ex) {
            final String message = String.format("Failed to evaluate XPath '%s'.", query);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public <E extends Node> NodeListCollection<E> evaluateXPath(final String query, final Node context, final Class<E> elementType) throws XmlUtilitiesException {
        return evaluateXPath(query, context, null, elementType);
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public <E extends Node> NodeListCollection<E> evaluateXPath(final String query, final Node context, final NamespaceContext namespaces, final Class<E> elementType)
            throws XmlUtilitiesException {
        NodeList nodes = evaluateXPath(query, context, namespaces);

        NodeListCollection<E> result = new NodeListCollection<E>(nodes, elementType);

        return result;
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     *
     * @return first node or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public Node getFirstNodeForXPath(final String query, final Node context) throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, (NamespaceContext) null);
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     *
     * @return first node or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public Node getFirstNodeForXPath(final String query, final Node context, final NamespaceContext namespaces) throws XmlUtilitiesException {
        final NodeList nodes = evaluateXPath(query, context, namespaces);

        if (nodes == null) {
            return null;
        }

        if (nodes.getLength() > 0) {
            return nodes.item(0);
        }

        return null;
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public <E extends Node> E getFirstNodeForXPath(final String query, final Node context, final Class<E> elementType) throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, null, elementType);
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public <E extends Node> E getFirstNodeForXPath(final String query, final Node context, final NamespaceContext namespaces, final Class<E> elementType)
            throws XmlUtilitiesException {
        Node node = getFirstNodeForXPath(query, context, namespaces);

        if (node == null) {
            return null;
        }

        final E element;
        try {
            element = elementType.cast(node);
        } catch (ClassCastException ex) {
            final String message = String.format("Failed to cast node from %s to %s.", node.getClass(), elementType);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }

        return element;
    }

    /**
     * Transforms specified XML source using specified XSLT template.
     *
     * @param xsltTemplate template to use
     * @param document source XML document
     * @param parameters parameters to propagate to the localTransformer
     *
     * @return resulting XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String transformToString(final Source xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        try {
            final Transformer localTransformer = transformerFactory.newTransformer(xsltTemplate);

            if (parameters != null) {
                for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
                    localTransformer.setParameter(parameter.getKey(), parameter.getValue());
                }
            }

            final StringWriter sw = new StringWriter();
            final StreamResult sr = new StreamResult(sw);

            localTransformer.transform(document, sr);

            final String result = sw.toString();

            try {
                sw.close();
            } catch (IOException ex) {
                return null;
            }

            return result;
        } catch (TransformerConfigurationException ex) {
            final String message = "Failed to transform Document using XSLT template and parameters.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (TransformerException ex) {
            final String message = "Failed to transform Document using XSLT template and parameters.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
    }

    /**
     * Transforms specified XML source using specified XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document source XML document to transform
     * @param parameters parameters to propagate to the localTransformer
     *
     * @return resulting XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String transformToString(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        try {
            final Transformer localTransformer = xsltTemplate.newTransformer();

            if (parameters != null) {
                for (Map.Entry<String, Object> parameter : parameters.entrySet()) {
                    localTransformer.setParameter(parameter.getKey(), parameter.getValue());
                }
            }

            final StringWriter sw = new StringWriter();
            final StreamResult sr = new StreamResult(sw);

            localTransformer.transform(document, sr);

            final String result = sw.toString();

            try {
                sw.close();
            } catch (IOException e) {
                return null;
            }

            return result;
        } catch (TransformerConfigurationException ex) {
            final String message = "Failed to transform Document using XSLT template and parameters.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        } catch (TransformerException ex) {
            final String message = "Failed to transform Document using XSLT template and parameters.";
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
    }

    /**
     * Transforms specified XML source using specified XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document source XML document to transform
     *
     * @return resulting XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String transformToString(final Templates xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, null);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     *
     * @return resulting XML document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public String transformToString(final Source xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, null);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     *
     * @return resulting XML document in the {@link Document Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public Document transformToDocument(final Source xsltTemplate, final Source document) throws XmlUtilitiesException {
        final XmlUtilities xmlUtils = new XmlUtilities();

        return XmlTools.loadDocumentFromString(transformToString(xsltTemplate, document, null), xmlUtils);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document document to transform
     *
     * @return resulting XML document in the {@link Document Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public Document transformToDocument(final Templates xsltTemplate, final Source document) throws XmlUtilitiesException {
        final XmlUtilities xmlUtils = new XmlUtilities();

        return XmlTools.loadDocumentFromString(transformToString(xsltTemplate, document, null), xmlUtils);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     * @param parameters parameters for the template
     *
     * @return resulting XML document in the {@link Document Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public Document transformToDocument(final Source xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        final XmlUtilities xmlUtils = new XmlUtilities();

        return XmlTools.loadDocumentFromString(transformToString(xsltTemplate, document, parameters), xmlUtils);
    }

    /**
     * Transforms specified XML using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param parameters parameters for the transformation
     *
     * @return resulting XML document in the {@link Document Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public Document transformToDocument(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        final XmlUtilities xmlUtils = new XmlUtilities();

        return XmlTools.loadDocumentFromString(transformToString(xsltTemplate, document, parameters), xmlUtils);
    }

    /**
     * Writes specified document into specified output stream.
     *
     * @param doc document
     * @param os output stream
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStream(final Document doc, final OutputStream os) throws XmlUtilitiesException {
        writeDocumentToStreamAndClose(doc, os, false, (Properties) null);
    }

    /**
     * Writes specified document into specified stream and specified output format.
     *
     * @param doc document
     * @param os output stream
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStream(final Document doc, final OutputStream os, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToStreamAndClose(doc, os, false, outputFormat);
    }

    /**
     * Writes specified document into specified stream and specified output format properties.
     *
     * @param doc document
     * @param os output stream
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStream(final Document doc, final OutputStream os, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToStreamAndClose(doc, os, false, outputProperties);
    }

    /**
     * Writes specified document into specified stream and specified {@link Transformer Transformer}.
     *
     * @param doc document
     * @param os output stream
     * @param transformer Transformer to use
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStream(final Document doc, final OutputStream os, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToStreamAndClose(doc, os, false, transformer);
    }

    /**
     * Writes specified document into specified output stream.
     *
     * @param doc document
     * @param os output stream
     * @param close tells if stream should be closed
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStreamAndClose(final Document doc, final OutputStream os, final boolean close) throws XmlUtilitiesException {
        writeDocumentToStreamAndClose(doc, os, close, (Properties) null);
    }

    /**
     * Writes specified document into specified stream and specified output format.
     *
     * @param doc document
     * @param os output stream
     * @param close tells if stream should be closed
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStreamAndClose(final Document doc, final OutputStream os, final boolean close, final OutputFormat outputFormat) throws XmlUtilitiesException {
        Properties outputProperties = null;
        if (outputFormat != null) {
            outputProperties = outputFormat.toTransformerOutputProperties();
        }
        writeDocumentToStreamAndClose(doc, os, close, outputProperties);
    }

    /**
     * Writes specified document into specified stream and specified output format properties.
     *
     * @param doc document
     * @param os output stream
     * @param close tells if stream should be closed
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStreamAndClose(final Document doc, final OutputStream os, final boolean close, final Properties outputProperties) throws XmlUtilitiesException {
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(os);

        XmlUtilitiesException exception = null;
        try {
            try {
                transformer.setParameter("", "");
                transformer.clearParameters();
                transformer.setOutputProperties(null);
                if (outputProperties != null) {
                    transformer.setOutputProperties(outputProperties);
                }
                transformer.transform(domSource, streamResult);
            } catch (TransformerException ex) {
                final String message = "Failed to write document to stream.";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            }
        } finally {
            if (close) {
                try {
                    os.close();
                } catch (IOException ex) {
                    final String message = "Failed to close stream.";
                    if (exception != null) {
                        LOGGER.log(Level.WARNING, message, ex);
                        exception.addConsequent(ex);
                    } else {
                        LOGGER.log(Level.SEVERE, message, ex);
                        exception = new XmlUtilitiesException(message, ex);
                    }
                }
            }

            if (exception != null) {
                throw exception;
            }
        }
    }

    /**
     * Writes specified document into specified stream and specified {@link Transformer Transformer}.
     *
     * @param doc document
     * @param os output stream
     * @param close tells if stream should be closed
     * @param transformer Transformer to use
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToStreamAndClose(final Document doc, final OutputStream os, final boolean close, final Transformer transformer) throws XmlUtilitiesException {
        DOMSource domSource = new DOMSource(doc);
        StreamResult streamResult = new StreamResult(os);

        XmlUtilitiesException exception = null;
        try {
            try {
                transformer.transform(domSource, streamResult);
            } catch (TransformerException ex) {
                final String message = "Failed to write document to stream.";
                LOGGER.log(Level.SEVERE, message, ex);
                exception = new XmlUtilitiesException(message, ex);
            }
        } finally {
            if (close) {
                try {
                    os.close();
                } catch (IOException ex) {
                    final String message = "Failed to close stream.";
                    if (exception != null) {
                        LOGGER.log(Level.WARNING, message, ex);
                        exception.addConsequent(ex);
                    } else {
                        LOGGER.log(Level.SEVERE, message, ex);
                        exception = new XmlUtilitiesException(message, ex);
                    }
                }
            }

            if (exception != null) {
                throw exception;
            }
        }
    }

    /**
     * Writes Document to stream using internal buffer and closes the stream.
     *
     * @param doc Document to write
     * @param os target stream
     * @param bufferSize specified buffer size, {@code null} for default
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToBufferedStreamAndClose(final Document doc, final OutputStream os, final Integer bufferSize) throws XmlUtilitiesException {
        final OutputStream output;
        if (bufferSize == null) {
            LOGGER.log(Level.FINEST, "Using default buffer size.");
            output = new BufferedOutputStream(os);
        } else {
            LOGGER.log(Level.FINEST, "Using buffer size {0}.", bufferSize);
            output = new BufferedOutputStream(os, bufferSize);
        }
        writeDocumentToStreamAndClose(doc, output, true);
    }

    /**
     * Writes Document to stream using internal buffer and closes the stream.
     *
     * @param doc Document to write
     * @param os target stream
     * @param bufferSize specified buffer size, {@code null} for default
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToBufferedStreamAndClose(final Document doc, final OutputStream os, final Integer bufferSize, final OutputFormat outputFormat)
            throws XmlUtilitiesException {
        final OutputStream output;
        if (bufferSize == null) {
            LOGGER.log(Level.FINEST, "Using default buffer size.");
            output = new BufferedOutputStream(os);
        } else {
            LOGGER.log(Level.FINEST, "Using buffer size {0}.", bufferSize);
            output = new BufferedOutputStream(os, bufferSize);
        }
        writeDocumentToStreamAndClose(doc, output, true, outputFormat);
    }

    /**
     * Writes Document to stream using internal buffer and closes the stream.
     *
     * @param doc Document to write
     * @param os target stream
     * @param bufferSize specified buffer size, {@code null} for default
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToBufferedStreamAndClose(final Document doc, final OutputStream os, final Integer bufferSize, final Properties outputProperties)
            throws XmlUtilitiesException {
        final OutputStream output;
        if (bufferSize == null) {
            LOGGER.log(Level.FINEST, "Using default buffer size.");
            output = new BufferedOutputStream(os);
        } else {
            LOGGER.log(Level.FINEST, "Using buffer size {0}.", bufferSize);
            output = new BufferedOutputStream(os, bufferSize);
        }
        writeDocumentToStreamAndClose(doc, output, true, outputProperties);
    }

    /**
     * Writes Document to stream using internal buffer and closes the stream.
     *
     * @param doc Document to write
     * @param os target stream
     * @param bufferSize specified buffer size, {@code null} for default
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToBufferedStreamAndClose(final Document doc, final OutputStream os, final Integer bufferSize, final Transformer transformer)
            throws XmlUtilitiesException {
        final OutputStream output;
        if (bufferSize == null) {
            LOGGER.log(Level.FINEST, "Using default buffer size.");
            output = new BufferedOutputStream(os);
        } else {
            LOGGER.log(Level.FINEST, "Using buffer size {0}.", bufferSize);
            output = new BufferedOutputStream(os, bufferSize);
        }
        writeDocumentToStreamAndClose(doc, output, true, transformer);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), (Integer) null);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), null, outputFormat);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), null, outputProperties);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), null, transformer);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), bufferSize);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), bufferSize, outputFormat);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), bufferSize, outputProperties);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, new File(file), bufferSize, transformer);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, (Integer) null);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, null, outputFormat);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, null, outputProperties);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, null, transformer);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize) throws XmlUtilitiesException {
        if (file == null) {
            throw new IllegalArgumentException("file can't be null.");
        }

        final OutputStream os;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            final String message = String.format("Failed to create output file '%s'.", file);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
        writeDocumentToBufferedStreamAndClose(doc, os, bufferSize);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final OutputFormat outputFormat) throws XmlUtilitiesException {
        if (file == null) {
            throw new IllegalArgumentException("file can't be null.");
        }

        final OutputStream os;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            final String message = String.format("Failed to create output file '%s'.", file);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
        writeDocumentToBufferedStreamAndClose(doc, os, bufferSize, outputFormat);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Properties outputProperties) throws XmlUtilitiesException {
        if (file == null) {
            throw new IllegalArgumentException("file can't be null.");
        }

        final OutputStream os;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            final String message = String.format("Failed to create output file '%s'.", file);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
        writeDocumentToBufferedStreamAndClose(doc, os, bufferSize, outputProperties);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Transformer transformer) throws XmlUtilitiesException {
        if (file == null) {
            throw new IllegalArgumentException("file can't be null.");
        }

        final OutputStream os;
        try {
            os = new FileOutputStream(file);
        } catch (FileNotFoundException ex) {
            final String message = String.format("Failed to create output file '%s'.", file);
            LOGGER.log(Level.SEVERE, message, ex);
            throw new XmlUtilitiesException(message, ex);
        }
        writeDocumentToBufferedStreamAndClose(doc, os, bufferSize, transformer);
    }

    /**
     * Disables logging of {@code XmlTools} and {@code XmlUtilities}.
     */
    public static void disableLogging() {
        disableLogging0();
        XmlTools.disableLogging0();
    }

    /**
     * Disables logging.
     */
    static void disableLogging0() {
        setLoggingLevel0(Level.OFF);
    }

    /**
     * Sets new logging level of {@code XmlTools} and {@code XmlUtilities}.
     *
     * @param newLevel level to set
     */
    public static void setLoggingLevel(final Level newLevel) {
        setLoggingLevel0(newLevel);
        XmlTools.setLoggingLevel0(newLevel);
    }

    /**
     * Sets new logging level.
     *
     * @param newLevel level to set
     */
    static void setLoggingLevel0(final Level newLevel) {
        LOGGER.setLevel(newLevel);
    }

}
