/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Static version of the XML utilities class.
 *
 * @author Petr Hadraba
 *
 * @version 1.2
 */
public final class XmlTools {

    /**
     * Internal logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XmlUtilities.XMLUTILITIES_LOGGER_NAME);

    /**
     * Use this constant, if you want to use internal XMLUtilities for the static methods.
     */
    private static final XmlUtilities INTERNAL_XMLUTILITIES = null;

    /**
     * Stores XMLUtilities object for the static usage.
     */
    private static XmlUtilities xmlUtilities = null;

    /**
     * Converts Document to DOMSource object.
     *
     * @param source Document XML representation
     *
     * @return converted XML source
     */
    public static Source documentToDomSource(final Document source) {
        return new DOMSource(source);
    }

    /**
     * Converts Document to DOMSource object.
     *
     * @param source Document XML representation
     * @param systemId system ID
     *
     * @return converted XML source
     */
    public static Source documentToDomSource(final Document source, String systemId) {
        return new DOMSource(source, systemId);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.documentToString(source);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.documentToString(source);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputFormat output format configuration
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source, final OutputFormat outputFormat) throws XmlUtilitiesException {
        return documentToString(source, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source, final Properties outputProperties) throws XmlUtilitiesException {
        return documentToString(source, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputFormat output format configuration
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source, final OutputFormat outputFormat, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.documentToString(source, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.documentToString(source, outputFormat);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source, final Properties outputProperties, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.documentToString(source, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.documentToString(source, outputProperties);
    }

    /**
     * Converts XML Document into String.
     *
     * @param source Document to convert
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String documentToString(final Document source) throws XmlUtilitiesException {
        return documentToString(source, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public static NodeList evaluateXPath(final String query, final Node context, final NamespaceContext namespaces, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.evaluateXPath(query, context, namespaces);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.evaluateXPath(query, context, namespaces);
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public static NodeList evaluateXPath(final String query, final Node context, final NamespaceContext namespaces) throws XmlUtilitiesException {
        return evaluateXPath(query, context, namespaces, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public static NodeList evaluateXPath(final String query, final Node context, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.evaluateXPath(query, context);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.evaluateXPath(query, context);
    }

    /**
     * Evaluates specified XPath expression on specified context node.
     *
     * @param query XPath expression
     * @param context context node
     *
     * @return {@link NodeList NodeList} or {@code null} if no matches
     *
     * @throws XmlUtilitiesException on error
     */
    public static NodeList evaluateXPath(final String query, final Node context) throws XmlUtilitiesException {
        return evaluateXPath(query, context, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public static <E extends Node> NodeListCollection<E> evaluateXPath(final String query, final Node context, final Class<E> elementType) throws XmlUtilitiesException {
        return evaluateXPath(query, context, elementType, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public static <E extends Node> NodeListCollection<E> evaluateXPath(final String query, final Node context, final NamespaceContext namespaces, final Class<E> elementType)
            throws XmlUtilitiesException {
        return evaluateXPath(query, context, namespaces, elementType, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     * @param xmlUtilities custom XMLUtilities; can be {@code null}
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public static <E extends Node> NodeListCollection<E> evaluateXPath(final String query, final Node context, final Class<E> elementType, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.evaluateXPath(query, context, elementType);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.evaluateXPath(query, context, elementType);
    }

    /**
     * Evaluates specified XPath expression on specified context node and returns results as
     * {@link NodeListCollection NodeListCollection} of specified element types. This method never returns {@code null}.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     * @param xmlUtilities custom XMLUtilities; can be {@code null}
     *
     * @return {@link NodeListCollection NodeListCollection} of element type or empty list if no matches.
     *
     * @throws XmlUtilitiesException on error
     */
    @SuppressWarnings("unchecked")
    public static <E extends Node> NodeListCollection<E> evaluateXPath(
            final String query,
            final Node context,
            final NamespaceContext namespaces,
            final Class<E> elementType,
            final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.evaluateXPath(query, context, namespaces, elementType);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.evaluateXPath(query, context, namespaces, elementType);
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     * @param xmlUtilities custom XMLUtilities; can be {@code null}
     *
     * @return first node or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Node getFirstNodeForXPath(final String query, final Node context, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.getFirstNodeForXPath(query, context);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.getFirstNodeForXPath(query, context);
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     *
     * @return first node or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Node getFirstNodeForXPath(final String query, final Node context) throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return first node or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Node getFirstNodeForXPath(final String query, final Node context, final NamespaceContext namespaces, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.getFirstNodeForXPath(
                    query, context, namespaces);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.getFirstNodeForXPath(
                query, context, namespaces);
    }

    /**
     * Evaluates specified XPath expression and returns first Node if XPath has matches.
     *
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     *
     * @return first node or null
     *
     * @throws XmlUtilitiesException on error
     */
    public static Node getFirstNodeForXPath(final String query, final Node context, final NamespaceContext namespaces) throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, namespaces, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static <E extends Node> E getFirstNodeForXPath(final String query, final Node context, final Class<E> elementType) throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, elementType, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static <E extends Node> E getFirstNodeForXPath(final String query, final Node context, final NamespaceContext namespaces, final Class<E> elementType)
            throws XmlUtilitiesException {
        return getFirstNodeForXPath(query, context, namespaces, elementType, INTERNAL_XMLUTILITIES);
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param elementType class type of element
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static <E extends Node> E getFirstNodeForXPath(final String query, final Node context, final Class<E> elementType, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.getFirstNodeForXPath(query, context, elementType);
        }

        if (XmlTools.xmlUtilities == null) {
            initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.getFirstNodeForXPath(query, context, elementType);
    }

    /**
     * Evaluates specified XPath expression and returns first Node as specified element type if XPath has matches.
     *
     * @param <E> type of element
     * @param query XPath expression
     * @param context context node
     * @param namespaces name space context
     * @param elementType class type of element
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return first node as specified type or {@code null}
     *
     * @throws XmlUtilitiesException on error
     */
    public static <E extends Node> E getFirstNodeForXPath(
            final String query,
            final Node context,
            final NamespaceContext namespaces,
            final Class<E> elementType,
            final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.getFirstNodeForXPath(query, context, namespaces, elementType);
        }

        if (XmlTools.xmlUtilities == null) {
            initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.getFirstNodeForXPath(query, context, namespaces, elementType);
    }

    /**
     * Initializes XMLUtilities static member for the static usage.
     *
     * @throws XmlUtilitiesException on error
     */
    private static void initializeXmlUtilities() throws XmlUtilitiesException {
        XmlTools.xmlUtilities = new XmlUtilities();
    }

    /**
     * Initializes XMLUtilities static member for the static usage using specified XMLUtilities.
     *
     * @param xmlUtilities XMLUtilities to use
     */
    public static void initializeXmlUtilities(final XmlUtilities xmlUtilities) {
        if (xmlUtilities == null) {
            throw new NullPointerException();
        }

        XmlTools.xmlUtilities = xmlUtilities;
    }

    /**
     * Rests internal XML Utilities so new instance will be created while next relevant call is invoked.
     */
    public static void resetXmlUtilities() {
        XmlTools.xmlUtilities = null;
    }

    /**
     * Returns SchemaFactory object internally used by the XMLUtilities static methods.
     *
     * @return SchemaFactory object
     */
    public static SchemaFactory getInternalSchemaFactory() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getSchemaFactory();
    }

    /**
     * Returns DocumentBuilder object internally used by the XMLUtilities static methods.
     *
     * @return DocumentBuilder object
     */
    public static DocumentBuilder getInternalDocumentBuilder() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getDocumentBuilder();
    }

    /**
     * Returns TransformerFactory object internally used by the XMLUtilities static methods.
     *
     * @return TransformerFactory object
     */
    public static TransformerFactory getInternalTransformerFactory() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getTransformerFactory();
    }

    /**
     * Returns Transformer object internally used by the XMLUtilities static methods.
     *
     * @return Transformer object
     */
    public static Transformer getInternalTransformer() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getTransformer();
    }

    /**
     * Returns XPathFactory object internally used by the XMLUtilities static methods.
     *
     * @return XPathFactory object
     */
    public static XPathFactory getInternalXPathFactory() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getXPathFactory();
    }

    /**
     * Returns NamespaceContext object internally used by the XMLUtilities static methods.
     *
     * @return NamespaceContext object
     */
    public static NamespaceContext getInternalNamespaceContext() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getDefaultNamespaceContext();
    }

    /**
     * Validates specified XML with the specified XMLScheme.
     *
     * @param xmlDocument XML document to validate
     * @param xmlSchema XMLSchema
     * @param xmlUtilities specified XMLUtilities. if null, internal will be used
     *
     * @return {@code null} if successful, string with error message otherwise
     *
     * @throws XmlUtilitiesException on technical error
     */
    public static String validateXmlUsingSchema(final Source xmlDocument, final Source xmlSchema, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.validateXmlUsingSchema(xmlDocument, xmlSchema);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.validateXmlUsingSchema(xmlDocument, xmlSchema);
    }

    /**
     * Validates specified XML with the specified XMLScheme.
     *
     * @param xmlDocument XML document to validate
     * @param xmlSchema XMLSchema
     *
     * @return {@code null} if successful, string with error otherwise
     *
     * @throws XmlUtilitiesException on technical error
     */
    public static String validateXmlUsingSchema(final Source xmlDocument, final Source xmlSchema) throws XmlUtilitiesException {
        return validateXmlUsingSchema(xmlDocument, xmlSchema, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     * @param xmlUtilities custom XMLUtilities object; can be null
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final String fileName, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromFile(fileName);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromFile(fileName);
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final String fileName) throws XmlUtilitiesException {
        return loadDocumentFromFile(fileName, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     * @param documentBuilder custom DocumentBuilder
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final String fileName, final DocumentBuilder documentBuilder, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromFile(fileName, documentBuilder);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromFile(fileName, documentBuilder);
    }

    /**
     * Loads XML from file specified with file name.
     *
     * @param fileName file to load
     * @param documentBuilder custom DocumentBuilder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final String fileName, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        return loadDocumentFromFile(fileName, documentBuilder, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from file specified with the File object.
     *
     * @param file file to load
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final File file, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromFile(file);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromFile(file);
    }

    /**
     * Loads XML from file specified with the File object.
     *
     * @param file file to load
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final File file) throws XmlUtilitiesException {
        return loadDocumentFromFile(file, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from file specified with the file object.
     *
     * @param file file to load
     * @param documentBuilder custom document builder
     * @param xmlUtilities custom XMLUtilities; can be null
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final File file, final DocumentBuilder documentBuilder, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromFile(file, documentBuilder);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromFile(file, documentBuilder);
    }

    /**
     * Loads XML from file specified with the file object.
     *
     * @param file file to load
     * @param documentBuilder custom document builder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromFile(final File file, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        return loadDocumentFromFile(file, documentBuilder, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from specified stream.
     *
     * @param is input stream
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromStream(final InputStream is, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromStream(is);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromStream(is);
    }

    /**
     * Loads XML from specified stream.
     *
     * @param is input stream
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromStream(final InputStream is) throws XmlUtilitiesException {
        return loadDocumentFromStream(is, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads XML from specified input stream.
     *
     * @param is input stream
     * @param documentBuilder custom Document Builder
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromStream(final InputStream is, final DocumentBuilder documentBuilder, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromStream(is, documentBuilder);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromStream(is, documentBuilder);
    }

    /**
     * Loads XML from specified input stream.
     *
     * @param is input stream
     * @param documentBuilder custom Document Builder
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromStream(final InputStream is, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        return loadDocumentFromStream(is, documentBuilder, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromClassLoader(final String resource, final Class<?> fallbackClazz) throws XmlUtilitiesException {
        return loadDocumentFromClassLoader(resource, fallbackClazz, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param documentBuilder custom DocumentBuilder
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromClassLoader(final String resource, final DocumentBuilder documentBuilder, final Class<?> fallbackClazz) throws XmlUtilitiesException {
        return loadDocumentFromClassLoader(resource, documentBuilder, fallbackClazz, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromClassLoader(final String resource, final Class<?> fallbackClazz, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromClassLoader(resource, fallbackClazz);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromClassLoader(resource, fallbackClazz);
    }

    /**
     * Loads document from ClassLoaders. The first of {@code ContextClassLoader}, {@code fallBackClazz#getClassLoader()}
     * or {@code SystemClassLoader} is used whichever is find first.
     *
     * @param resource resource name to load from
     * @param documentBuilder custom DocumentBuilder
     * @param fallbackClazz ClassLoader to use if {@code ContextClassLoader} does not exist
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromClassLoader(
            final String resource,
            final DocumentBuilder documentBuilder,
            final Class<?> fallbackClazz,
            final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromClassLoader(resource, documentBuilder, fallbackClazz);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromClassLoader(resource, documentBuilder, fallbackClazz);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param clazz class to use
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final Class<?> clazz, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromResource(resource, clazz);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromResource(resource, clazz);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param clazz class to use
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final Class<?> clazz, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromResource(resource, documentBuilder, clazz);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromResource(resource, documentBuilder, clazz);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param clazz class to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final Class<?> clazz) throws XmlUtilitiesException {
        return loadDocumentFromClassLoader(resource, clazz, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads Document from resource from given class.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param clazz class to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final Class<?> clazz) throws XmlUtilitiesException {
        return loadDocumentFromResource(resource, documentBuilder, clazz, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param classLoader ClassLoader to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final ClassLoader classLoader) throws XmlUtilitiesException {
        return loadDocumentFromResource(resource, classLoader, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param classLoader ClassLoader to use
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final ClassLoader classLoader) throws XmlUtilitiesException {
        return loadDocumentFromResource(resource, documentBuilder, classLoader, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param classLoader ClassLoader to use
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final ClassLoader classLoader, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromResource(resource, classLoader);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromResource(resource, classLoader);
    }

    /**
     * Loads document from resource using specified ClassLoader.
     *
     * @param resource resource to load
     * @param documentBuilder custom DocumentBuilder
     * @param classLoader ClassLoader to use
     * @param xmlUtilities custom XMLUtilities (can be null)
     *
     * @return Document object
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromResource(final String resource, final DocumentBuilder documentBuilder, final ClassLoader classLoader, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromResource(resource, documentBuilder, classLoader);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromResource(resource, documentBuilder, classLoader);
    }

    /**
     * Returns DocumentBuilderFactory object internally used by the XMLUtilities static methods.
     *
     * @return DocumentBuilderFactory object
     */
    public static DocumentBuilderFactory getInternalDocumentBuilderFactory() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getDocumentBuilderFactory();
    }

    /**
     * Returns XPath object internally used by the XMLUtilities static methods.
     *
     * @return XPath object
     */
    public static XPath getInternalXPath() {
        if (xmlUtilities == null) {
            return null;
        }

        return xmlUtilities.getXPath();
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate template to use
     * @param document source XML document
     *
     * @return resulting document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Source xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     * @param xmlUtilities custom {@code XMLUtilities}
     *
     * @return resulting XML document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Source xsltTemplate, final Source document, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, null, xmlUtilities);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template to use
     * @param document source XML document
     * @param parameters parameters for the XSLT processor
     *
     * @return resulting XML file in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Source xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, parameters, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML document using specified XSLT template.
     *
     * @param xsltTemplate XSLT template to use
     * @param document source XML document
     * @param parameters parameters for the XSLT processor
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Source xsltTemplate, final Source document, final Map<String, Object> parameters, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.transformToString(xsltTemplate, document, parameters);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.transformToString(xsltTemplate, document, parameters);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param parameters parameters for the transformation
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.transformToString(xsltTemplate, document, parameters);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.transformToString(xsltTemplate, document, parameters);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param parameters parameters for the transformation
     *
     * @return resulting XML document in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, parameters, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Templates xsltTemplate, final Source document, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, null, xmlUtilities);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     *
     * @return resulting XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String transformToString(final Templates xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToString(xsltTemplate, document, null, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param parameters parameters for the transformation
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.transformToDocument(xsltTemplate, document, parameters);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.transformToDocument(xsltTemplate, document, parameters);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param parameters parameters for the transformation
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Templates xsltTemplate, final Source document, final Map<String, Object> parameters) throws XmlUtilitiesException {
        return transformToDocument(xsltTemplate, document, parameters, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Templates xsltTemplate, final Source document, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        return transformToDocument(xsltTemplate, document, null, xmlUtilities);
    }

    /**
     * Transforms specified XML document using XSLT (compiled) template.
     *
     * @param xsltTemplate template to use
     * @param document XML document to transform
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Templates xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToDocument(xsltTemplate, document, null, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromString(final String source) throws XmlUtilitiesException {
        return loadDocumentFromString(source, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromString(final String source, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromString(source);
        }

        if (XmlTools.xmlUtilities == null) {
            initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromString(source);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     * @param documentBuilder custom document builder
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromString(final String source, final DocumentBuilder documentBuilder) throws XmlUtilitiesException {
        return loadDocumentFromString(source, documentBuilder, INTERNAL_XMLUTILITIES);
    }

    /**
     * Loads document from String into the {@code Document}.
     *
     * @param source source XML
     * @param documentBuilder custom DocumentBuilder to use
     * @param xmlUtilities custom {@code XMLUtilities} to use
     *
     * @return resulting XML
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document loadDocumentFromString(final String source, final DocumentBuilder documentBuilder, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.loadDocumentFromString(source, documentBuilder);
        }

        if (XmlTools.xmlUtilities == null) {
            initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.loadDocumentFromString(source, documentBuilder);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source) throws XmlUtilitiesException {
        return sourceToString(source, XmlTools.INTERNAL_XMLUTILITIES);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     * @param xmlUtilities {@code XMLUtilities} to use
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.sourceToString(source);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.sourceToString(source);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     * @param outputFormat output format configuration
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source, final OutputFormat outputFormat) throws XmlUtilitiesException {
        return sourceToString(source, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source, final Properties outputProperties) throws XmlUtilitiesException {
        return sourceToString(source, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     * @param outputFormat output format configuration
     * @param xmlUtilities {@code XMLUtilities} to use
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source, final OutputFormat outputFormat, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.sourceToString(source, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.sourceToString(source, outputFormat);
    }

    /**
     * Converts XML Source into String.
     *
     * @param source source Document
     * @param outputProperties output properties (see {@link OutputFormat OutputFormat})
     * @param xmlUtilities {@code XMLUtilities} to use
     *
     * @return XML in the String
     *
     * @throws XmlUtilitiesException on error
     */
    public static String sourceToString(final Source source, final Properties outputProperties, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.sourceToString(source, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.sourceToString(source, outputProperties);
    }

    /**
     * Transforms specified XML using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     * @param parameters parameters for the template
     * @param xmlUtilities {@code XMLUtilities} to use
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Source xsltTemplate, final Source document, final Map<String, Object> parameters, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.transformToDocument(xsltTemplate, document, parameters);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.transformToDocument(xsltTemplate, document, parameters);
    }

    /**
     * Transforms specified XML using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     * @param parameters parameters for the template
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Source xsltTemplate, final Source document, final Map<String, Object> parameters)
            throws XmlUtilitiesException {
        return transformToDocument(xsltTemplate, document, parameters, INTERNAL_XMLUTILITIES);
    }

    /**
     * Transforms specified XML using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     * @param xmlUtilities {@code XMLUtilities} to use
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Source xsltTemplate, final Source document, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            return xmlUtilities.transformToDocument(xsltTemplate, document);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        return XmlTools.xmlUtilities.transformToDocument(xsltTemplate, document);
    }

    /**
     * Transforms specified XML using specified XSLT template.
     *
     * @param xsltTemplate XSLT template
     * @param document source XML document
     *
     * @return resulting XML in the {@code Document}
     *
     * @throws XmlUtilitiesException on error
     */
    public static Document transformToDocument(final Source xsltTemplate, final Source document) throws XmlUtilitiesException {
        return transformToDocument(xsltTemplate, document, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes specified document into specified output stream.
     *
     * @param doc document
     * @param os output stream
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os) throws XmlUtilitiesException {
        writeDocumentToStream(doc, os, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes specified document into specified output stream.
     *
     * @param doc document
     * @param os output stream
     * @param xmlUtilities XMLUtilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToStream(doc, os);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToStream(doc, os);
    }

    /**
     * Writes specified document into specified stream and specified output format.
     *
     * @param doc document
     * @param os output stream
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToStream(doc, os, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes specified document into specified stream and specified output format.
     *
     * @param doc document
     * @param os output stream
     * @param outputFormat output format
     * @param xmlUtilities XMLUtilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final OutputFormat outputFormat, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToStream(doc, os, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToStream(doc, os, outputFormat);
    }

    /**
     * Writes specified document into specified stream and specified output format properties.
     *
     * @param doc document
     * @param os output stream
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToStream(doc, os, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes specified document into specified stream and specified output format properties.
     *
     * @param doc document
     * @param os output stream
     * @param outputProperties output format properties
     * @param xmlUtilities XMLUtilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final Properties outputProperties, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToStream(doc, os, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToStream(doc, os, outputProperties);
    }

    /**
     * Writes specified document into specified stream and specified {@link Transformer Transformer}.
     *
     * @param doc document
     * @param os output stream
     * @param transformer Transformer to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToStream(doc, os, transformer, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes specified document into specified stream and specified {@link Transformer Transformer}.
     *
     * @param doc document
     * @param os output stream
     * @param transformer Transformer to use
     * @param xmlUtilities XMLUtilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToStream(final Document doc, final OutputStream os, final Transformer transformer, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToStream(doc, os, transformer);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToStream(doc, os, transformer);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, transformer, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, transformer, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final OutputFormat outputFormat, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, outputFormat);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Properties outputProperties, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, outputProperties);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Transformer transformer, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, transformer);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, transformer);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final OutputFormat outputFormat, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputFormat);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Properties outputProperties, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputProperties);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final String file, final Integer bufferSize, final Transformer transformer, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, transformer);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, transformer);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, transformer, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final OutputFormat outputFormat) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, outputFormat, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Properties outputProperties) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, outputProperties, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Transformer transformer) throws XmlUtilitiesException {
        writeDocumentToFile(doc, file, bufferSize, transformer, INTERNAL_XMLUTILITIES);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputFormat output format
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final OutputFormat outputFormat, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, outputFormat);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param outputProperties output format properties
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Properties outputProperties, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, outputProperties);
    }

    /**
     * Writes Document to specified file.
     *
     * @param doc document
     * @param file target file
     * @param transformer transformer
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Transformer transformer, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, transformer);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, transformer);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final XmlUtilities xmlUtilities) throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputFormat output format
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final OutputFormat outputFormat, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputFormat);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputFormat);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param outputProperties output format properties
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Properties outputProperties, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputProperties);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, outputProperties);
    }

    /**
     * Writes Document to specified file using internal buffer.
     *
     * @param doc document
     * @param file target file
     * @param bufferSize buffer size or {@code null} for default
     * @param transformer transformer
     * @param xmlUtilities XML Utilities to use
     *
     * @throws XmlUtilitiesException on error
     */
    public static void writeDocumentToFile(final Document doc, final File file, final Integer bufferSize, final Transformer transformer, final XmlUtilities xmlUtilities)
            throws XmlUtilitiesException {
        if (xmlUtilities != null) {
            xmlUtilities.writeDocumentToFile(doc, file, bufferSize, transformer);
        }

        if (XmlTools.xmlUtilities == null) {
            XmlTools.initializeXmlUtilities();
        }

        XmlTools.xmlUtilities.writeDocumentToFile(doc, file, bufferSize, transformer);
    }

    /**
     * Loads XML from String.
     *
     * @param source source string
     *
     * @return XML in the {@link Source Source}
     */
    public static Source loadSourceFromString(final String source) {
        return new StreamSource(new StringReader(source));
    }

    /**
     * Loads XML from String.
     *
     * @param source source string
     * @param systemId system ID
     *
     * @return XML in the {@link Source Source}
     */
    public static Source loadSourceFromString(final String source, final String systemId) {
        return new StreamSource(new StringReader(source), systemId);
    }

    /**
     * Disables logging of {@code XmlTools} and {@code XmlUtilities}.
     */
    public static void disableLogging() {
        disableLogging0();
        XmlUtilities.disableLogging0();
    }

    /**
     * Disables logging.
     */
    static void disableLogging0() {
        setLoggingLevel0(Level.OFF);
    }

    /**
     * Sets new logging level of {@code XmlTools} and {@code XmlUtilities}.
     *
     * @param newLevel level to set
     */
    public static void setLoggingLevel(final Level newLevel) {
        setLoggingLevel0(newLevel);
        XmlUtilities.setLoggingLevel0(newLevel);
    }

    /**
     * Sets new logging level.
     *
     * @param newLevel level to set
     */
    static void setLoggingLevel0(final Level newLevel) {
        LOGGER.setLevel(newLevel);
    }

    /**
     * Utility class.
     */
    private XmlTools() {
        throw new AssertionError(String.format("No instance of %s for you!", XmlTools.class.getName()));
    }

}
