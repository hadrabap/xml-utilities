/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;

/**
 * Simple implementation of the NamespaceContext.
 *
 * @author Petr Hadraba
 *
 * @version 1.1
 */
public class NamespaceContextImpl implements NamespaceContext {

    /**
     * Factory method which creates empty name space context <em>without</em> defaults.
     *
     * @return empty name space context
     */
    public static NamespaceContextImpl emptyNamespaceContext() {
        return new NamespaceContextImpl(null);
    }

    /**
     * Factory method which creates empty name space context <em>with</em> defaults.
     *
     * @return new name space context with defaults
     */
    public static NamespaceContextImpl namespaceContextWithDefaults() {
        return new NamespaceContextImpl();
    }

    /**
     * Stores name space mappings.
     */
    private final Map<String, String> namespaces = new HashMap<String, String>(4);

    /**
     * Creates a new instance of NamespaceContextImpl and adds default name space URIs.
     */
    public NamespaceContextImpl() {
        addDefaultNamespaces0();
    }

    /**
     * Internal constructor for empty context without defaults.
     *
     * @param v {@code null}
     */
    private NamespaceContextImpl(Void v) {
        // do not populate defaults
    }

    /**
     * Adds name space URI specified with prefix and URI.
     *
     * @param prefix prefix of the name space
     * @param namespaceURI URI of the name space
     */
    public void addNamespace(String prefix, String namespaceURI) {
        namespaces.put(prefix, namespaceURI);
    }

    /**
     * Adds all the name space URIs that are stored in the specified Map.
     *
     * @param namespaceURIs URIs to add, key is prefix, value is URI
     */
    public void addNamespaces(Map<String, String> namespaceURIs) {
        namespaces.putAll(namespaceURIs);
    }

    /**
     * Adds default name space URIs.
     *
     * Default name space URIs are {@code DEFAULT_NS_PREFIX}, {@code XML_NS_PREFIX} and {@code XMLNS_ATTRIBUTE}.
     */
    public void addDefaultNamespaces() {
        addDefaultNamespaces0();
    }

    /**
     * Adds default name space URIs.
     *
     * Default name space URIs are {@code DEFAULT_NS_PREFIX}, {@code XML_NS_PREFIX} and {@code XMLNS_ATTRIBUTE}.
     */
    private void addDefaultNamespaces0() {
        namespaces.put(XMLConstants.DEFAULT_NS_PREFIX, XMLConstants.NULL_NS_URI);
        namespaces.put(XMLConstants.XML_NS_PREFIX, XMLConstants.XML_NS_URI);
        namespaces.put(XMLConstants.XMLNS_ATTRIBUTE, XMLConstants.XMLNS_ATTRIBUTE_NS_URI);
    }

    /**
     * Removes specified name space specified by prefix.
     *
     * @param prefix of the name space
     */
    public void removeNamespaceByPrefix(String prefix) {
        namespaces.remove(prefix);
    }

    /**
     * Removes specified name space specified by URI.
     *
     * @param uri URI of the name space to remove
     */
    public void removeNamespaceByURI(String uri) {
        final Iterator<Map.Entry<String, String>> it = namespaces.entrySet().iterator();

        while (it.hasNext()) {
            if (it.next().getValue().equals(uri)) {
                it.remove();
            }
        }
    }

    /**
     * Removes all the name space URIs including default ones.
     */
    public void removeAllNamespaces() {
        namespaces.clear();
    }

    /**
     * Returns name space URI that corresponds to the specified prefix.
     *
     * @param prefix URI of the name space to obtain
     *
     * @return URI of the name space or {@code NULL_NS_URI} if not found
     */
    @Override
    public String getNamespaceURI(String prefix) {
        if (prefix == null) {
            throw new IllegalArgumentException("Prefix can't be null.");
        }

        final String uri = namespaces.get(prefix);

        if (uri == null) {
            return XMLConstants.NULL_NS_URI;
        }

        return uri;
    }

    /**
     * Returns the prefix for the corresponding name space URI.
     *
     * @param namespaceURI name space URI
     *
     * @return prefix for the URI or {@code null} if not found
     */
    @Override
    public String getPrefix(String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException("namespaceURI can't be null.");
        }

        final Iterator<Map.Entry<String, String>> it = namespaces.entrySet().iterator();

        while (it.hasNext()) {
            final Map.Entry<String, String> mapping = it.next();
            if (mapping.getValue().equals(namespaceURI)) {
                return mapping.getKey();
            }
        }

        return null;
    }

    /**
     * Returns iterator for the set of all the prefixes that correspond to the specified URI.
     *
     * @param namespaceURI name space URI
     *
     * @return set of prefixes (iterator) or empty set
     */
    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        if (namespaceURI == null) {
            throw new IllegalArgumentException("namespaceURI can't be null.");
        }

        final ArrayList<String> prefixes = new ArrayList<String>(1);

        final Iterator<Map.Entry<String, String>> it = namespaces.entrySet().iterator();

        while (it.hasNext()) {
            final Map.Entry<String, String> mapping = it.next();
            if (mapping.getValue().equals(namespaceURI)) {
                prefixes.add(mapping.getKey());
            }
        }

        return prefixes.iterator();
    }

}
