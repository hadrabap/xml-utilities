## Version 1.3.1

The version 1.3.1 reflects migration from Mercurial to GIT.


## Version 1.3

The version 1.3 is a bug-fix release.


## Version 1.2

The 1.2 version has moved from Java 1.5 to Java 1.6. The 1.2 version is _not_ compatible with version 1.1. There are also additional improvements such as:

 - Transition to Bitbucket
 - Transition to Maven
 - Code clean-up
 - Generics support for XPath evaluation
 - Load-from-Resource methods
 - Store functionality (Streams, Files)
 - Output format properties
 - Common checked exception
 - Additional tests

For more information with usage examples please visit [README](https://bitbucket.org/hadrabap/xml-utilities/src/xmlutilities-1.2/README.md) file.
