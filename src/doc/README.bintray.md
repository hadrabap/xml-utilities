
XML Utilities
=============

Copyright © 2006, 2017 Petr Hadraba <hadrabap@gmail.com>

## About

XML Utilities is a class path library with no (transitive) dependencies which makes essential XML-related
tasks easier. In version 1.2 (similar to previous versions), everything is related to DOM operations.

The primary goal is to provide easy helper methods to manipulate small XML documents; usually configuration files.

## Loading

XML Utilities allows to load XML Documents from various sources.

### File System

```
Document doc = XmlTools.loadDocumentFromFile("/path/to/file");
```

### Stream

```
Document doc = XmlTools.loadDocumentFromStream(inputStream);
```

### Class Path Resource

```
Document doc = XmlTools.loadDocumentFromResource("resource-name", this.getClass());
```

### String

```
Document doc = XmlTools.loadDocumentFromString("<?xml version...");
```

## Writing

XML Utilities allow writing Documents into various destinations.

### File System

```
XmlTools.writeDocumentToFile(document, file);
```

### Stream

```
XmlTools.writeDocumentToStream(document, outputStream);
```

## Conversions

XML Utilities library allows to convert DOM Document into DOM Source or String.

```
String s = XmlTools.documentToString(document);
```

## XPath Evaluation

XML Utilities library provides two simple variants of XPath evaluation:

 * List oriented and
 * single result.

XPath-related functions support generics (as of version 1.2):

```
List<Element> result = XmlTools.evaluateXPath("xpath/query()", node, Element.class);
```

## XSD Validation

XML Utilities supports validation of Documents against XML Schema:

```
String result = XmlTools.validateXmlUsingSchema(documentSource, xsdSource);
```

The result of the method denotes text of error message if the validation fails.

## Transformation

XML Utilities library has also support for XSLT transformations:

```
Document result = XmlTools.transformToDocument(xslTemplate, documentSource, parameters);
```

## Documentation

The complete documentation is covered in Javadoc. There is also a `demo` package and simple test cases for reference.

## Installation

The XML Utilities starting from version 1.2 uses Apache Maven. The build system uses the following profiles:

 * `strict`: forces build and JUnit tests to be run using JDK8.
 * `release`: the complete build for distribution; forces usage of JDK6.
 * `jarsigner`: optional JAR signing mechanism.

The target class level is JDK6. The appropriate version of JDK is controlled by Maven Toolchain Plugin.
