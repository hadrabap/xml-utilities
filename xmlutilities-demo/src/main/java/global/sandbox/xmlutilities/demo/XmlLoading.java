/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.transform.Source;
import org.w3c.dom.Document;

public class XmlLoading {

    public static Document loadXmlFromResourceAsDocument(Class<?> clazz, String name) throws NoSuchResourceException, XmlLoadingException {
        InputStream is = clazz.getResourceAsStream(name);

        if (is == null) {
            throw new NoSuchResourceException(name);
        }

        try {
            return XmlTools.loadDocumentFromStream(is);
        } catch (XmlUtilitiesException ex) {
            throw new XmlLoadingException(ex);
        } finally {
            try {
                is.close();
            } catch (IOException ex) {
                throw new XmlLoadingException(ex);
            }
        }
    }

    public static Source loadXmlFromResourceAsSource(Class<?> clazz, String name) throws NoSuchResourceException, XmlLoadingException {
        return XmlTools.documentToDomSource(loadXmlFromResourceAsDocument(clazz, name));
    }

    public static Document loadXmlFromResourceAsDocument12(Class<?> clazz, String name) throws XmlLoadingException {
        try {
            return XmlTools.loadDocumentFromResource(name, clazz);
        } catch (XmlUtilitiesException ex) {
            throw new XmlLoadingException(ex);
        }
    }

    public static Source loadXmlFromResourceAsSource12(Class<?> clazz, String name) throws XmlLoadingException {
        return XmlTools.documentToDomSource(loadXmlFromResourceAsDocument12(clazz, name));
    }

    public static class NoSuchResourceException extends Exception {

        private static final long serialVersionUID = 1L;

        public NoSuchResourceException(String resourceName) {
            super(resourceName);
        }

    }

    public static class XmlLoadingException extends Exception {

        private static final long serialVersionUID = 1L;

        public XmlLoadingException(Throwable cause) {
            super(cause);
        }

    }

}
