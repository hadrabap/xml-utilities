/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import javax.xml.transform.Source;
import org.w3c.dom.Document;

public class SchemaValidator {

    public static void validateXml(Source doc, Source schema) throws SchemaValidatorException, ValidationException {
        String result;

        try {
            result = XmlTools.validateXmlUsingSchema(doc, schema);
        } catch (XmlUtilitiesException ex) {
            throw new SchemaValidatorException(ex);
        }

        if (result != null) {
            throw new ValidationException(result);
        }
    }

    public static void validateXml(Document doc, Document schema) throws SchemaValidatorException, ValidationException {
        SchemaValidator.validateXml(XmlTools.documentToDomSource(doc), XmlTools.documentToDomSource(schema));
    }

    public static class SchemaValidatorException extends Exception {

        private static final long serialVersionUID = 1L;

        public SchemaValidatorException(Throwable cause) {
            super(cause);
        }

    }

    public static class ValidationException extends Exception {

        private static final long serialVersionUID = 1L;

        public ValidationException(String message) {
            super(message);
        }

    }

}
