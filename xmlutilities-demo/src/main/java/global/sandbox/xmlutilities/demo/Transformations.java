/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import org.w3c.dom.Document;

public class Transformations {

    public static Document transform(Document doc, Document xsl) throws TransformationsException {
        try {
            return XmlTools.transformToDocument(
                    XmlTools.documentToDomSource(xsl),
                    XmlTools.documentToDomSource(doc));
        } catch (XmlUtilitiesException ex) {
            throw new TransformationsException(ex);
        }
    }

    public static class TransformationsException extends Exception {

        private static final long serialVersionUID = 1L;

        public TransformationsException(Throwable cause) {
            super(cause);
        }

    }

}
