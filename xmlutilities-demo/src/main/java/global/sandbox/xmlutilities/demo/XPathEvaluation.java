/*
 * Common usable utilities
 *
 * Copyright (c) 2006 Petr Hadraba <hadrabap@gmail.com>
 *
 * Author: Petr Hadraba
 *
 * --
 *
 * XML Utilities
 */

package global.sandbox.xmlutilities.demo;

import global.sandbox.xmlutilities.NamespaceContextImpl;
import global.sandbox.xmlutilities.XmlTools;
import global.sandbox.xmlutilities.XmlUtilitiesException;
import javax.xml.namespace.NamespaceContext;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class XPathEvaluation {

    final static NamespaceContext NAMESPACES;

    static {
        final NamespaceContextImpl nspaces = NamespaceContextImpl.namespaceContextWithDefaults();
        nspaces.addNamespace(
                "ns",
                "http://hadrabap.googlepages.com/projects/xmlutilities/demo");

        NAMESPACES = nspaces;
    }

    public static Attr loadAttribute(Element rootNode, String xPathQuery) throws XPathEvaluationException {
        Node node;
        try {
            node = XmlTools.getFirstNodeForXPath(
                    xPathQuery,
                    rootNode,
                    NAMESPACES);
        } catch (XmlUtilitiesException ex) {
            throw new XPathEvaluationException(ex);
        }

        if (node == null) {
            return null;
        }

        if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
            return (Attr) node;
        } else {
            throw new XPathEvaluationException(String.format("Resulting node is not Attr, got %d.", node.getNodeType()));
        }
    }

    public static Attr loadAttribute12(Element rootNode, String xPathQuery) throws XmlUtilitiesException {
        return XmlTools.getFirstNodeForXPath(
                xPathQuery,
                rootNode,
                NAMESPACES,
                Attr.class);
    }

    public static class XPathEvaluationException extends Exception {

        private static final long serialVersionUID = 1L;

        public XPathEvaluationException(String message) {
            super(message);
        }

        public XPathEvaluationException(Throwable cause) {
            super(cause);
        }

    }

}
